﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameStatsController
{

    public static GameStats CurrentGameStats;

    public const string FileName = "GameStats.json";

    public static string FilePath { get { return Application.persistentDataPath + "/" + FileName; } }



    public static void SaveStats()
    {
        if (CurrentGameStats == null)
        {
            LoadStats();
        }
        StreamWriter file = new StreamWriter(FilePath, false);
        file.Write(JsonUtility.ToJson(CurrentGameStats));
        file.Close();
    }

    public static void LoadStats()
    {
        try
        {
            string jsonStats = File.ReadAllText(FilePath);
            if (string.IsNullOrEmpty(jsonStats))
            {
                Debug.Log("File was empty or nulled");
                CurrentGameStats = new GameStats();
            }
            else
            {
                GameStats statsFromFile = JsonUtility.FromJson<GameStats>(jsonStats);
                CurrentGameStats = statsFromFile;
                Debug.Log("Succesfully loaded stats from file");
            }
        }
        catch (Exception e)
        {
            CurrentGameStats = new GameStats();
            Debug.Log("Something went wrong when reading file : " + e.Message);
            return;
        }
    }


    public static void KilledEnemy()
    {
        CurrentGameStats.EnemiesKilled++;
        AchievementsController.EnemyKilled();

        if (GameEventsManager.Get.EnemiesFrozen)
            AchievementsController.FrozenEnemyKilled();
    }

    public static void DestroyedSpiderNest()
    {
        CurrentGameStats.SpiderNestsDestroyed++;
        AchievementsController.SpiderNestDestroyed();
    }

    public static void DestroyedExplosiveBarrel()
    {
        CurrentGameStats.ExplosiveBarrelsDestroyed++;
        AchievementsController.BarrelDestroyed();
    }

    public static void CollectableTaken()
    {
        CurrentGameStats.CollectablesTaken++;
        AchievementsController.CollectableTaken();
    }
}
