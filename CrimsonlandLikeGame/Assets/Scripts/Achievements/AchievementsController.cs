﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Main controller for managing achievements in game, saving and loading to file, methods to increase achievements  values 
/// </summary>
public class AchievementsController
{

    public const string FileName = "Achievements.json";

    public static string FilePath { get { return Application.persistentDataPath + "/" + FileName; } }

    /// <summary>
    /// Current achievements list, loaded from file at game start and saved when game is over
    /// </summary>
    public static AchievementsList CurrentAchievements;

    /// <summary>
    /// Saves current achievements to file as json
    /// </summary>
    public static void SaveAchievements()
    {
        if (CurrentAchievements == null)
        {
            CurrentAchievements = new AchievementsList();
        }
        if (CurrentAchievements.Achievements == null)
        {
            CurrentAchievements.Achievements = CreateNewAchievements();
        }
        StreamWriter file = new StreamWriter(FilePath, false);
        file.Write(JsonUtility.ToJson(CurrentAchievements));
        file.Close();
    }

    /// <summary>
    /// Tries to load achievements from file. If there is no file or something is wrong, achievements are created as new.
    /// </summary>
    public static void LoadAchievements()
    {
        try
        {
            string jsonStats = File.ReadAllText(FilePath);
            if (string.IsNullOrEmpty(jsonStats))
            {
                Debug.Log("File was empty or nulled");
                CurrentAchievements = new AchievementsList();
                CurrentAchievements.Achievements = CreateNewAchievements();
            }
            else
            {
                AchievementsList achievementsFromFile = JsonUtility.FromJson<AchievementsList>(jsonStats);
                CurrentAchievements = achievementsFromFile;
                Debug.Log("Succesfully loaded achievements from file");
            }
        }
        catch (Exception e)
        {
            CurrentAchievements = new AchievementsList();
            CurrentAchievements.Achievements = CreateNewAchievements();

            Debug.Log("Something went wrong when reading file : " + e.Message);
            return;
        }


    }

    /// <summary>
    /// Invoked when enemy is killed
    /// </summary>
    public static void EnemyKilled()
    {
        foreach (Achievement achievement in CurrentAchievements.Achievements)
        {
            if (achievement.AchievementType != EAchievementType.EnemiesKilled)
                continue;
            if (achievement.CurrentValue >= achievement.TargetValue)
                continue;
            achievement.AddValue(1);
        }
    }

    /// <summary>
    /// Inoked when player killed enemy when EnemiesFrozen is active
    /// </summary>
    public static void FrozenEnemyKilled()
    {
        foreach (Achievement achievement in CurrentAchievements.Achievements)
        {
            if (achievement.AchievementType != EAchievementType.FrozenEnemiesKilled)
                continue;
            if (achievement.CurrentValue >= achievement.TargetValue)
                continue;
            achievement.AddValue(1);
        }
    }

    /// <summary>
    /// Invoked when player destroys spiders nest
    /// </summary>
    public static void SpiderNestDestroyed()
    {
        foreach (Achievement achievement in CurrentAchievements.Achievements)
        {
            if (achievement.AchievementType != EAchievementType.SpiderNestsKilled)
                continue;
            if (achievement.CurrentValue >= achievement.TargetValue)
                continue;
            achievement.AddValue(1);
        }
    }

    /// <summary>
    /// Invoked when player destroys explosive barrel
    /// </summary>
    public static void BarrelDestroyed()
    {
        foreach (Achievement achievement in CurrentAchievements.Achievements)
        {
            if (achievement.AchievementType != EAchievementType.ExplosiveBarrelsDestroyed)
                continue;
            if (achievement.CurrentValue >= achievement.TargetValue)
                continue;
            achievement.AddValue(1);
        }
    }

    /// <summary>
    /// Invoked at end of survival with time player spent in this game mode
    /// </summary>
    /// <param name="time"></param>
    public static void SpentTimeInSurvival(int time)
    {
        foreach (Achievement achievement in CurrentAchievements.Achievements)
        {
            if (achievement.AchievementType != EAchievementType.TimeSpentInSurvivalMode)
                continue;
            if (achievement.CurrentValue >= achievement.TargetValue)
                continue;
            achievement.AddValue(time);
        }
    }

    /// <summary>
    /// Invoked when player collects a collectable
    /// </summary>
    public static void CollectableTaken()
    {
        foreach (Achievement achievement in CurrentAchievements.Achievements)
        {
            if (achievement.AchievementType != EAchievementType.CollectablesTaken)
                continue;
            if (achievement.CurrentValue >= achievement.TargetValue)
                continue;
            achievement.AddValue(1);
        }
    }

    /// <summary>
    /// Invoked when enemy is set on fire. Only once for one enemy
    /// </summary>
    public static void EnemySetOnFire()
    {
        foreach (Achievement achievement in CurrentAchievements.Achievements)
        {
            if (achievement.AchievementType != EAchievementType.EnemiesSetOnFire)
                continue;
            if (achievement.CurrentValue >= achievement.TargetValue)
                continue;
            achievement.AddValue(1);
        }
    }
    
    /// <summary>
    /// Invoked when kamikaze spider explodes and kills other enemies with its explosion
    /// </summary>
    /// <param name="value"></param>
    public static void EnemyKilledWithExplosiveSpider(int value)
    {
        foreach (Achievement achievement in CurrentAchievements.Achievements)
        {
            if (achievement.AchievementType != EAchievementType.EnemiesKilledWithExplosiveSpider)
                continue;
            if (achievement.CurrentValue >= achievement.TargetValue)
                continue;
            achievement.AddValue(value);
        }
    }

    /// <summary>
    /// Invoked when used sniper bullet with count of enemies killed with this shot
    /// </summary>
    /// <param name="count"></param>
    public static void EnemiesKilledWithSniperShot(int count)
    {
        foreach (Achievement achievement in CurrentAchievements.Achievements)
        {
            if (achievement.AchievementType != EAchievementType.EnemiesKilledWithOneSniperShot)
                continue;
            if (achievement.CurrentValue >= achievement.TargetValue)
                continue;

            if (count >= achievement.TargetValue)
                achievement.AddValue(count);
        }
    }

    /// <summary>
    /// Creates new achievements list
    /// </summary>
    /// <returns></returns>
    public static List<Achievement> CreateNewAchievements()
    {
        List<Achievement> achievements = new List<Achievement>()
        {
            new Achievement(EAchievementType.EnemiesKilled,0,10,"Getting started!", "Kill 10 spiders"),
            new Achievement(EAchievementType.EnemiesKilled,0,100,"Killing time!", "Kill 100 spiders"),
            new Achievement(EAchievementType.EnemiesKilled,0,500,"Kill them all!", "Kill 500 spiders"),
            new Achievement(EAchievementType.EnemiesKilled,0,1000,"Can you stop please?", "Kill 1000 spiders"),

            new Achievement(EAchievementType.SpiderNestsKilled,0,5,"That's where they come from!", "Destroy 5 spider nests"),
            new Achievement(EAchievementType.SpiderNestsKilled,0,20,"I dont like those eggs!", "Destroy 20 spider nests"),
            new Achievement(EAchievementType.SpiderNestsKilled,0,50,"Destroy it!", "Destroy 50 spider nests"),
            new Achievement(EAchievementType.SpiderNestsKilled,0,100,"Master of destruction!", "Destroy 100 spider nests"),

            new Achievement(EAchievementType.ExplosiveBarrelsDestroyed,0,10,"Do they explode?", "Destroy 10 explosive barrels"),
            new Achievement(EAchievementType.ExplosiveBarrelsDestroyed,0,50,"Boom!", "Destroy 50 explosive barrels"),
            new Achievement(EAchievementType.ExplosiveBarrelsDestroyed,0,100,"Fireworks!", "Destroy 100 explosive barrels"),
            new Achievement(EAchievementType.ExplosiveBarrelsDestroyed,0,250,"Are you Michael Bay?", "Destroy 250 explosive barrels"),

            new Achievement(EAchievementType.CollectablesTaken,0,10,"Delicious pills!", "Collect 10 pills"),
            new Achievement(EAchievementType.CollectablesTaken,0,100,"Can I have more, please?", "Collect 100 pills"),
            new Achievement(EAchievementType.CollectablesTaken,0,250,"That's some nice collection!", "Collect 250 pills"),
            new Achievement(EAchievementType.CollectablesTaken,0,500,"Open your drugstore now!", "Collect 500 pills"),

            new Achievement(EAchievementType.EnemiesKilledWithExplosiveSpider,0,5,"Not my fault!", "Kill 5 other spiders with spider explosion"),
            new Achievement(EAchievementType.EnemiesKilledWithExplosiveSpider,0,25,"I did not kill it!", "Kill 25 other spiders with spider explosion"),
            new Achievement(EAchievementType.EnemiesKilledWithExplosiveSpider,0,100,"That's impressive!", "Kill 100 other spiders with spider explosion"),
            new Achievement(EAchievementType.EnemiesKilledWithExplosiveSpider,0,250,"Those were all accidents!", "Kill 250 other spiders with spider explosion"),

            new Achievement(EAchievementType.EnemiesKilledWithOneSniperShot,0,2,"Two for one!", "Kill 2 spiders with one sniper shot"),
            new Achievement(EAchievementType.EnemiesKilledWithOneSniperShot,0,3,"Head-trick!", "Kill 3 spiders with one sniper shot"),
            new Achievement(EAchievementType.EnemiesKilledWithOneSniperShot,0,5,"Pentakill!", "Kill 5 spiders with one sniper shot"),
            new Achievement(EAchievementType.EnemiesKilledWithOneSniperShot,0,10,"Woah, just wow!", "Kill 10 spiders with one sniper shot"),

            new Achievement(EAchievementType.TimeSpentInSurvivalMode,0,60,"I will survive!", "Spend a minute in survival mode"),
            new Achievement(EAchievementType.TimeSpentInSurvivalMode,0,600,"Walk in the park!", "Spend 10 minutes in survival mode"),
            new Achievement(EAchievementType.TimeSpentInSurvivalMode,0,1800,"Half an hour already!", "Spend 30 minutes in survival mode"),
            new Achievement(EAchievementType.TimeSpentInSurvivalMode,0,3600,"Isn't that boring?!", "Spend 60 minutes in survival mode"),

            new Achievement(EAchievementType.FrozenEnemiesKilled,0,10,"So cold!", "Kill 10 frozen enemies"),
            new Achievement(EAchievementType.FrozenEnemiesKilled,0,100,"Dont move!", "Kill 100 frozen enemies"),
            new Achievement(EAchievementType.FrozenEnemiesKilled,0,250,"Ice cube!", "Kill 250 frozen enemies"),
            new Achievement(EAchievementType.FrozenEnemiesKilled,0,500,"Frozen!", "Kill 500 frozen enemies"),

            new Achievement(EAchievementType.EnemiesSetOnFire,0,10,"Firestarter", "Set 10 enemies on fire"),
            new Achievement(EAchievementType.EnemiesSetOnFire,0,100,"It's getting hot!", "Set 100 enemies on fire"),
            new Achievement(EAchievementType.EnemiesSetOnFire,0,250,"Burn!", "Set 250 enemies on fire"),
            new Achievement(EAchievementType.EnemiesSetOnFire,0,500,"Killed it with fire!", "Set 500 enemies on fire"),
        };

        return achievements;
    }
}
