﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameStats
{
    public int EnemiesKilled = 0;

    public int SpiderNestsDestroyed = 0;

    public int ExplosiveBarrelsDestroyed = 0;

    public int CollectablesTaken = 0;

    public int LevelsCompleted=0;
}
