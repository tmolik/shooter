﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Wrapper class for achievements list as there would be problems with jsonUtility and serialization of List of achievements if it weren't in class
/// </summary>
[System.Serializable]
public class AchievementsList
{
    public List<Achievement> Achievements;
}
