﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class representing single achievement
/// </summary>
[System.Serializable]
public class Achievement
{

    public EAchievementType AchievementType;

    public int CurrentValue;

    public int TargetValue;

    public string AchievementName;

    public string Description;

    public DateTime AchievedAt;

    public bool IsAchieved { get { return CurrentValue >= TargetValue; } }

    public Achievement(EAchievementType achievementType, int currentValue, int targetValue, string achievementName, string description)
    {
        AchievementType = achievementType;
        CurrentValue = currentValue;
        TargetValue = targetValue;
        AchievementName = achievementName;
        Description = description;
    }

    /// <summary>
    /// Adds value to current achievement value and unlocks it if it is achieved
    /// </summary>
    /// <param name="value"></param>
    public void AddValue(int value)
    {
        CurrentValue += value;
        if (CurrentValue >= TargetValue)
        {
            UiNewAchievementController.Get.AddAchievementToQueue(this);
            AchievedAt = DateTime.Now;
            UiAchievementsList.Get.AchievementUnlocked(this);
        }
    }
}
