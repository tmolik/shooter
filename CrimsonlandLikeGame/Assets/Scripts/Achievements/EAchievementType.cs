﻿/// <summary>
/// Types of achievements
/// </summary>
public enum EAchievementType
{
    EnemiesKilled,
    SpiderNestsKilled,
    CollectablesTaken,
    ExplosiveBarrelsDestroyed,
    EnemiesKilledWithExplosiveSpider,
    EnemiesKilledWithOneSniperShot,
    TimeSpentInSurvivalMode,
    FrozenEnemiesKilled,
    EnemiesSetOnFire
}
