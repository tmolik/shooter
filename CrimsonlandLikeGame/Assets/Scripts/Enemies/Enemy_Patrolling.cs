﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Patrolling : EnemyController
{

    public Vector3 CurrentDestination;

    public bool IsAtDestination = false;

    public float TimeToSpendAtDestination = 5;

    public PatrollingPath PathToFollow;

    protected int CurrentPointIndex = 0;

    protected float AtDestinationTimer = 0;

    // Use this for initialization
    public new void Start()
    {
        base.Start();
        IsAtDestination = true;

    }


    // Update is called once per frame
    public new void Update()
    {
        /*This must go like that as base update function isnt Updated*/
        if (IsOnFire)
        {
            if (IsAlive)
                OnHit(2 * Time.deltaTime);

            if (FireTransform != null)
                FireTransform.position = transform.position + (FireOffset != null ? FireOffset.Offset : Vector3.zero);

            OnFireTimer += Time.deltaTime;

            if (OnFireTimer > 5)
            {
                IsOnFire = false;
                //FireTransform.gameObject.SetActive(false);
                ParticleSystem ps = FireTransform.transform.GetComponent<ParticleSystem>();
                var emission = ps.emission;
                var rate = emission.rateOverTime;
                rate.constantMax = 0f;
                emission.rateOverTime = rate;
                StartCoroutine(DisableFireTransform());
            }
        }

        if (IsAlive)
        {
            EnemyAnimator.SetFloat("Speed", WalkAnimationSpeedMultiplier);

            /* Calculations of enemy being pushed away */
            if (IsBeingPushed)
            {
                transform.position += PushDirection * 15 * Time.deltaTime;

                PushHelperTime += Time.deltaTime;

                if (PushHelperTime > PushTime)
                {
                    IsBeingPushed = false;
                }
            }
            else if (IsGettingHit)
            {
                getHitTimer += Time.deltaTime;

                if (getHitTimer > 0.1f)
                {
                    if (EnemyAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f)
                    {
                        IsGettingHit = false;
                        StartMoving();
                    }
                }
            }
            else if (GameEventsManager.Get.EnemiesFrozen)
            {
                EnemyAnimator.SetFloat("Speed", 0);

                if (IceTransform != null)
                    IceTransform.position = transform.position + (FireOffset != null ? FireOffset.Offset : Vector3.zero) + Vector3.up;
                else
                {
                    GameObject ice = ObjectPooler.Get.GetPooledObject("Ice");
                    //ice.transform.localScale = transform.localScale;
                    ice.transform.position = transform.position + (FireOffset != null ? FireOffset.Offset : Vector3.zero) + Vector3.up;
                    ice.gameObject.SetActive(true);
                    IceTransform = ice.transform;
                }

            }
            else
            {

                if (IceTransform != null)
                {
                    IceTransform.gameObject.SetActive(false);
                    IceTransform = null;
                }

                if (!IsAtDestination)
                {
                    if (!IsMoving)
                        StartMoving();

                    if (NavAgent == null)
                    {
                        transform.position += (CurrentDestination - transform.position).normalized * MoveSpeed * 4 * Time.deltaTime;
                        transform.position = new Vector3(transform.position.x, 0, transform.position.z);

                        Quaternion targetRotation = Quaternion.LookRotation(CurrentDestination - transform.position);
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 30);
                    }
                    else
                    {
                        NavAgent.SetDestination(CurrentDestination);

                        if (NavAgent.path.status == UnityEngine.AI.NavMeshPathStatus.PathComplete && NavAgent.path.corners.Length >= 2)
                        {
                            Vector3 navDirection = (NavAgent.path.corners[1] - transform.position).normalized;
                            transform.position += navDirection * MoveSpeed * 4 * Time.deltaTime;
                            transform.position = new Vector3(transform.position.x, 0, transform.position.z);

                            Quaternion targetRotation = Quaternion.LookRotation(navDirection);
                            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 30);
                        }
                    }


                    if (Vector3.Distance(transform.position, CurrentDestination) < 2)
                    {
                        ReachDestination();
                    }
                }
                else
                {
                    AtDestinationTimer += Time.deltaTime;
                    AtDestinationAction();
                    if (AtDestinationTimer >= TimeToSpendAtDestination)
                    {
                        SpentEnoughTimeAtDestination();
                    }
                }

            }
        }
        else
        {
            HelperTimer += Time.deltaTime;
            if (HelperTimer >= 240f)
            {
                Destroy(gameObject);
            }
        }
    }

    public virtual void AtDestinationAction()
    {

    }

    public virtual void SpentEnoughTimeAtDestination()
    {
        SetNewDestination();
    }

    public void SetNewDestination()
    {
        IsAtDestination = false;
        if (PathToFollow != null && PathToFollow.PathPoints.Count > 1)
        {
            int nextIndex = CurrentPointIndex + 1;
            if (nextIndex <= PathToFollow.PathPoints.Count - 1)
            {
                CurrentDestination = PathToFollow.PathPoints[nextIndex];
                CurrentDestination = new Vector3(CurrentDestination.x, transform.position.y, CurrentDestination.z);
                CurrentPointIndex = nextIndex;
            }
            else
            {
                CurrentDestination = PathToFollow.PathPoints[0];
                CurrentDestination = new Vector3(CurrentDestination.x, transform.position.y, CurrentDestination.z);
                CurrentPointIndex = 0;
            }
        }
        else
        {
            Vector3 newPos = new Vector3(Random.Range(-30, 30), transform.position.y, Random.Range(-30, 30));
            CurrentDestination = newPos;
        }

        if (NavAgent != null)
            NavAgent.SetDestination(CurrentDestination);

    }

    public virtual void ReachDestination()
    {
        IsAtDestination = true;
        AtDestinationTimer = 0;
        IsMoving = false;
        EnemyAnimator.CrossFade("Idle", 0.4f);
    }

}
