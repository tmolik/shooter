﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Base controller for enemies that moves towards player
/// </summary>
public class EnemyController : HittableObject
{
    public float WalkAnimationSpeedMultiplier = 1;

    /// <summary> How much experience will player get for killing this enemy</summary>
    public int Experience;

    /// <summary> How fast does this enemy move </summary>
    public float MoveSpeed;

    /// <summary> How often can this enemy attack</summary>
    public float AttackTime;

    /// <summary> Time of last attack by this enemy</summary>
    public float LastAttackTime;

    /// <summary> How many damage will every attack deal</summary>
    public float AttackDamage;

    /// <summary> How close does it need to be to player to attack</summary>
    public float AttackRange;

    /* Helper variables for enemy being pushed away by collectable */

    /// <summary> Is pushing away in progress</summary>
    internal bool IsBeingPushed;

    internal float PushHelperTime;

    /// <summary> How much time will pushing away take</summary>
    internal float PushTime;

    /// <summary> Direction in which enemy is pushed </summary>
    internal Vector3 PushDirection;

    public Animator EnemyAnimator;

    public NavMeshAgent NavAgent;

    public bool AlwaysGoToPlayer = false;

    public AudioSource EnemyAudioSource;

    internal Vector3 CurrentDestinationPosition;
    private float destinationHelperTimer;
    internal bool isCloseToPlayer = false;
    public float ChangeDestinationTime;

    internal bool IsMoving = false;
    internal bool IsAttacking = false;
    internal bool IsGettingHit;
    internal float HelperTimer;

    private float audioHelperTimer = 0;
    internal float getHitTimer = 0;

    public new void Start()
    {
        base.Start();
        StartCoroutine(AddToEnemies());
        EnemyAudioSource = gameObject.AddComponent<AudioSource>();
        NavAgent = GetComponent<NavMeshAgent>();
        gameObject.AddComponent<AudioSpeedMatcher>();
        EnemyAnimator.SetFloat("Speed", WalkAnimationSpeedMultiplier);
        SetDestination();
        ChangeDestinationTime = 2;
    }

    private IEnumerator AddToEnemies()
    {
        yield return new WaitForSeconds(1);

        if (!IsAlive)
            yield break;

        LevelInfo level = LevelInfo.Get;
        if (level != null)
        {
            level.Enemies.Add(this);
        }
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();

        if (MainController.LoadingLevel || MainController.IsPreparingLevel)
            return;

        if (IsAlive)
        {
            EnemyAnimator.SetFloat("Speed", WalkAnimationSpeedMultiplier);

            destinationHelperTimer += Time.deltaTime;
            if (destinationHelperTimer >= ChangeDestinationTime)
            {
                SetDestination();
                destinationHelperTimer = 0;
            }

            /* Calculations of enemy being pushed away */
            if (IsBeingPushed)
            {
                transform.position += PushDirection * 15 * Time.deltaTime;

                PushHelperTime += Time.deltaTime;

                if (PushHelperTime > PushTime)
                {
                    IsBeingPushed = false;
                }
            }
            else if (IsGettingHit)
            {
                getHitTimer += Time.deltaTime;

                if (getHitTimer > 0.1f)
                {
                    if (EnemyAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f)
                    {
                        IsGettingHit = false;
                        StartMoving();
                    }
                }
            }
            else if (GameEventsManager.Get.EnemiesFrozen)
            {
                EnemyAnimator.SetFloat("Speed", 0);

                if (IceTransform != null)
                    IceTransform.position = transform.position + (FireOffset != null ? FireOffset.Offset : Vector3.zero) + Vector3.up;
                else
                {
                    GameObject ice = ObjectPooler.Get.GetPooledObject("Ice");
                    //ice.transform.localScale = transform.localScale;
                    ice.transform.position = transform.position + (FireOffset != null ? FireOffset.Offset : Vector3.zero) + Vector3.up;
                    ice.gameObject.SetActive(true);
                    IceTransform = ice.transform;
                }

            }
            else
            {
                if (IceTransform != null)
                {
                    IceTransform.gameObject.SetActive(false);
                    IceTransform = null;
                }

                if (PlayerController.Get.Parameters.IsAlive)
                {
                    /* Calculations of enemy moving towards player */
                    if (!IsAttacking)
                    {
                        if (Vector3.Distance(transform.position, PlayerController.Get.transform.position) > AttackRange)
                        {
                            if (!IsMoving)
                                StartMoving();

                            if (NavAgent == null)
                            {
                                if (isCloseToPlayer)
                                {
                                    transform.position += (PlayerController.Get.transform.position - transform.position).normalized * MoveSpeed * 4 * Time.deltaTime;
                                    transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                                }
                                else
                                {
                                    transform.position += (CurrentDestinationPosition - transform.position).normalized * MoveSpeed * 4 * Time.deltaTime;
                                    transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                                }
                            }
                            else
                            {
                                if (isCloseToPlayer)
                                {
                                    NavAgent.SetDestination(PlayerController.Get.transform.position);
                                }
                                else
                                {
                                    NavAgent.SetDestination(CurrentDestinationPosition);
                                }

                                if (NavAgent.path.status == NavMeshPathStatus.PathComplete && NavAgent.path.corners.Length >= 2)
                                {
                                    Vector3 navDirection = (NavAgent.path.corners[1] - transform.position).normalized;
                                    transform.position += navDirection * MoveSpeed * 4 * Time.deltaTime;
                                    transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                                }
                            }
                        }

                        Vector3 lookRotation;
                        if (isCloseToPlayer)
                        {
                            lookRotation = new Vector3(PlayerController.Get.transform.position.x, 0, PlayerController.Get.transform.position.z);
                        }
                        else
                        {
                            lookRotation = new Vector3(CurrentDestinationPosition.x, 0, CurrentDestinationPosition.z);
                        }
                        Quaternion targetRotation = Quaternion.LookRotation(lookRotation - transform.position);
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 120 * Time.deltaTime);

                        AttackUpdate();
                    }
                    else
                    {
                        if (EnemyAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f)
                        {
                            IsAttacking = false;
                            EnemyAnimator.CrossFade("Idle", 0.4f);
                        }
                    }
                }
                else
                {
                    EnemyAnimator.CrossFade("Idle", 0.4f);
                }
            }

            TryPlaySound();

        }
        else
        {
            HelperTimer += Time.deltaTime;
            if (HelperTimer >= LevelInfo.Get.DestroyEnemiesAfter)
            {
                Destroy(gameObject);
            }
        }
    }

    public virtual void SetDestination()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, PlayerController.Get.transform.position);
        isCloseToPlayer = false;
        if (distanceToPlayer < 10 || AlwaysGoToPlayer)
        {
            CurrentDestinationPosition = PlayerController.Get.transform.position;
            isCloseToPlayer = true;
        }
        else if (distanceToPlayer < 30)
        {
            CurrentDestinationPosition = PlayerController.Get.transform.position + new Vector3(Random.Range(-20, 20), 0, Random.Range(-20, 20));
        }
        else
        {
            CurrentDestinationPosition = PlayerController.Get.transform.position + new Vector3(Random.Range(-50, 50), 0, Random.Range(-50, 50));

        }
        if (NavAgent != null)
            NavAgent.SetDestination(CurrentDestinationPosition);
    }

    public void TryPlaySound()
    {
        audioHelperTimer += Time.deltaTime;
        if (audioHelperTimer > 10)
        {
            int rand = Random.Range(0, 100);
            if (rand <= 30)
                EnemyAudioSource.PlayOneShot(AudioManager.Get.SpiderClips.GetRandomAudioClip());
            else if (rand <= 60)
                EnemyAudioSource.PlayOneShot(AudioManager.Get.SpiderChatClips.GetRandomAudioClip());

            audioHelperTimer = 0;
        }
    }

    /// <summary>
    /// Checks if can attack again and if player is in attack range. If yes, he attacks
    /// </summary>
    public void AttackUpdate()
    {
        if (Time.time - LastAttackTime >= AttackTime)
        {
            if (Vector3.Distance(transform.position, PlayerController.Get.transform.position) < AttackRange)
            {
                Attack();
            }
        }
    }

    public void StartMoving()
    {
        EnemyAnimator.CrossFade("Walk", 0.4f);
        IsMoving = true;
    }

    /// <summary>
    /// Invoked when hit with bullet
    /// </summary>
    /// <param name="damage"></param>
    public override bool OnHit(float damage)
    {
        base.OnHit(damage);
        if (damage > 1)
        {
            GameObject bloodSplat = Instantiate(PrefabsHelper.Get.BloodSplatPrefab);
            bloodSplat.transform.position = new Vector3(transform.position.x, 0.01f, transform.position.z);
            bloodSplat.transform.rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
            bloodSplat.transform.SetParent(LevelInfo.Get.transform);
            if (IsAlive)
            {
                getHitTimer = 0;
                IsGettingHit = true;
                EnemyAnimator.CrossFade("GetHit", 0.1f);

                if (Random.Range(0, 100) > 70)
                    EnemyAudioSource.PlayOneShot(AudioManager.Get.SpiderClips.GetRandomAudioClip());
                else
                    EnemyAudioSource.PlayOneShot(AudioManager.Get.EnemyGetHitClip);


            }
        }
        return IsAlive;
    }

    public new void OnDestroy()
    {
        base.OnDestroy();
        LevelInfo level = LevelInfo.Get;
        if (level != null && level.Enemies.Contains(this))
        {
            level.Enemies.Remove(this);
        }
    }

    /// <summary>
    /// Starts enemy pushing away
    /// </summary>
    /// <param name="explosionPosition"></param>
    public void Push(Vector3 explosionPosition)
    {
        if (NavAgent != null)
            NavAgent.isStopped = true;

        IsBeingPushed = true;
        IsMoving = false;
        IsAttacking = false;
        PushHelperTime = 0;
        PushDirection = (transform.position - explosionPosition).normalized;
        PushDirection.y = 0;
        PushTime = Mathf.Lerp(1, 0, Vector3.Distance(transform.position, explosionPosition) / 5);
        EnemyAnimator.CrossFade("Pushed", 0.1f);
    }

    /// <summary>
    /// Enemy attacks player
    /// </summary>
    public virtual void Attack()
    {
        IsMoving = false;
        IsAttacking = true;
        PlayerController.Get.Parameters.Attacked(AttackDamage);
        LastAttackTime = Time.time;
        EnemyAnimator.CrossFade("Attack", 0.01f);
    }

    /// <summary>
    /// Function invoked when object's health is zero, by default is destroys itself
    /// </summary>
    public override void OnZeroHealth()
    {
        Collider collider = GetComponent<Collider>();
        if (collider) collider.enabled = false;
        EnemyAnimator.CrossFade("Death", 0.05f);

        LevelInfo level = LevelInfo.Get;
        if (level != null && level.Enemies.Contains(this))
        {
            level.Enemies.Remove(this);
        }

        if (NavAgent != null)
            NavAgent.isStopped = true;

        EnemyAudioSource.PlayOneShot(AudioManager.Get.SpiderClips.GetRandomAudioClip());
        GameStatsController.KilledEnemy();
        PlayerController.Get.Parameters.GainExp(Experience);

        if (Random.Range(0, 100) > 65)
        {
            CollectablesManager.Get.SpawnCollectable(transform.position);
        }

        if (IceTransform != null)
        {
            IceTransform.gameObject.SetActive(false);
            IceTransform = null;
        }

        NavAgent.enabled = false;
    }

    private bool wasOnceSetOnFire = false;

    public override bool SetOnFire()
    {
        bool setOnFire = base.SetOnFire();
        if (setOnFire && !wasOnceSetOnFire)
        {
            wasOnceSetOnFire = true;
            AchievementsController.EnemySetOnFire();
        }
        return setOnFire;
    }

}
