﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Mother : Enemy_Patrolling
{
    public GameObject EnemyToSpawnAfterDeath;
    public int AfterDeathSpawnCount;
    public int DefendersCount;

    private float spawnTimer;

    public new void Start()
    {
        base.Start();
        for(int i = 0; i < DefendersCount; i++)
        {
            CreateDefender();
        }
    }

    public new void Update()
    {
        base.Update();

        if (IsAlive)
        {
            spawnTimer += Time.deltaTime;
            if(spawnTimer>=10f)
            {
                CreateDefender();
                spawnTimer = 0;
            }
        }
        
    }


    public override void OnZeroHealth()
    {
        for (int i = 0; i < AfterDeathSpawnCount; i++)
        {
            GameObject enemy = Instantiate(EnemyToSpawnAfterDeath);
            enemy.transform.position = transform.position;
            enemy.transform.SetParent(LevelInfo.Get.transform);
        }
        base.OnZeroHealth();
 
    }
        
    public void CreateDefender()
    {
        Enemy_MotherDefender defender = Instantiate(PrefabsHelper.Get.SpiderMotherDefender).GetComponent<Enemy_MotherDefender>();
        defender.transform.position = transform.position;
        defender.transform.SetParent(LevelInfo.Get.transform);
        defender.Mother = this;
    }

}
