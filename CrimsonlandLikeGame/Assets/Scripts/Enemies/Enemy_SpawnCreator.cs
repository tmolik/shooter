﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_SpawnCreator : Enemy_Patrolling
{
    public GameObject EnemyToSpawn;

    // Use this for initialization
    public new void Start()
    {
        base.Start();
        IsAtDestination = false;
        TimeToSpendAtDestination = 10;
    }

    public override void SpentEnoughTimeAtDestination()
    {
        base.SpentEnoughTimeAtDestination();

        if (EnemyToSpawn != null)
        {
            EnemySpawner spawner = Instantiate(PrefabsHelper.Get.EnemySpawnerPrefab).GetComponent<EnemySpawner>();
            spawner.EnemiesToRespawn =new List<GameObject> { EnemyToSpawn };
            spawner.transform.position = transform.position;
        }
    }

    public override void ReachDestination()
    {
        base.ReachDestination();
    }
}
