﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_MotherDefender : EnemyController
{
    public Enemy_Mother Mother;

    public new void Start()
    {
        base.Start();
        ChangeDestinationTime = 1;
    }

    public override void SetDestination()
    {
        if (Mother == null || (Mother!=null && !Mother.IsAlive))
        {
            base.SetDestination();
            return;
        }

        float distanceToPlayer = Vector3.Distance(transform.position, PlayerController.Get.transform.position);
        isCloseToPlayer = false;
        if (distanceToPlayer < 10)
        {
            CurrentDestinationPosition = PlayerController.Get.transform.position;
            isCloseToPlayer = true;
        }
        else
        {
            CurrentDestinationPosition = Mother.transform.position + new Vector3(Random.Range(-20, 20), 0, Random.Range(-20, 20));
        }
    }



}
