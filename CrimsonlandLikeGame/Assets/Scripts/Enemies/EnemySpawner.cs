﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple enemy spawner that spawns enemies as long as it's alive
/// </summary>
public class EnemySpawner : HittableObject
{
    /// <summary>Prefab of enemy to respawn</summary>
    public List<GameObject> EnemiesToRespawn = new List<GameObject>();

    public bool ShouldSpawn = true;

    /// <summary>How often should it spawn new enemy in seconds</summary>
    public float RespawnTime;

    public GameObject DestroyParticle;

    /// <summary>Helper timer for respawning enemies</summary>
    private float helperRespawnTime;


    public new void Start()
    {
        base.Start();
        StartCoroutine(AddToSpawners());
    }

    private IEnumerator AddToSpawners()
    {
        yield return new WaitForSeconds(1);
        LevelInfo level = LevelInfo.Get;
        if (level != null)
        {
            level.Spawners.Add(this);
        }
    }


    // Update is called once per frame
    public override void Update()
    {
        base.Update();

        if (!ShouldSpawn)
            return;

        helperRespawnTime += Time.deltaTime;
        if (helperRespawnTime >= RespawnTime)
        {
            if (EnemiesToRespawn.Count > 0)
            {
                GameObject enemyToRespawn = EnemiesToRespawn[Random.Range(0, EnemiesToRespawn.Count)];
                Vector3 position= new Vector3(transform.position.x, 0, transform.position.z);
                EnemyController enemy = Instantiate(enemyToRespawn,position,Quaternion.identity).GetComponent<EnemyController>();
                //enemy.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                enemy.transform.SetParent(LevelInfo.Get.transform);
                helperRespawnTime = 0;
            }
        }
    }

    public void SetSpawning(bool spawn)
    {
        ShouldSpawn = spawn;
    }

    public override void OnZeroHealth()
    {
        GameStatsController.DestroyedSpiderNest();
        AudioManager.Get.PlayClipAtPosition(transform.position, AudioManager.Get.SpiderNestDestroyClip,0.5f);
        base.OnZeroHealth();
        if (DestroyParticle != null)
        {
            GameObject particle = Instantiate(DestroyParticle);
            particle.transform.position = transform.position;
        }

        GameObject destroyedNest = Instantiate(PrefabsHelper.Get.DestroyedSpiderNest);
        destroyedNest.transform.position = transform.position;
        destroyedNest.transform.rotation = transform.rotation;
        destroyedNest.transform.SetParent(LevelInfo.Get.transform);

    }

    public new void OnDestroy()
    {
        base.OnDestroy();
        LevelInfo level = LevelInfo.Get;
        if (level != null)
        {
            level.Spawners.Remove(this);
        }
    }
}
