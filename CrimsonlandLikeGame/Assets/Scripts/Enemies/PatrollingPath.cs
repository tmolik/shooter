﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrollingPath : MonoBehaviour
{
    public List<Vector3> PathPoints = new List<Vector3>();

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i <= transform.childCount-1; i++)
        {
            Vector3 pos = transform.GetChild(i).position;
            PathPoints.Add(pos);
        }
    }

}
