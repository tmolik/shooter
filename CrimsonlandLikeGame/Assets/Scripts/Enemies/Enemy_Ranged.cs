﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Ranged : EnemyController
{
    public string BulletPoolableTag;

    /// <summary>
    /// Enemy attacks player
    /// </summary>
    public override void Attack()
    {
        IsMoving = false;
        IsAttacking = true;
        LastAttackTime = Time.time;
        EnemyAnimator.CrossFade("Attack", 0.01f);

        BulletController bullet = ObjectPooler.Get.GetPooledObject(BulletPoolableTag).GetComponent<BulletController>();

        if (bullet == null)
            return;

        bullet.transform.position = transform.position + transform.forward * 2 + Vector3.up*1.8f;
        bullet.transform.rotation = Quaternion.Euler(transform.forward);
        bullet.Direction = transform.forward.normalized;
        bullet.Damage = AttackDamage;
        bullet.gameObject.SetActive(true);
    }
}
