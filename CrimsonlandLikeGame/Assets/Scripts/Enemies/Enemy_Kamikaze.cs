﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enemy that runs in player direction and explodes when is near
/// </summary>
public class Enemy_Kamikaze : EnemyController
{

    /// <summary>
    /// Enemy attacks player
    /// </summary>
    public override void Attack()
    {
        SpawnExplosion();

        Destroy(gameObject);
    }


    /// <summary>
    /// Invoked when killed 
    /// </summary>
    public override void OnZeroHealth()
    {
        SpawnExplosion();
        base.OnZeroHealth();

    }

    /// <summary>
    /// Spawn explosion efect and invoke Explode method
    /// </summary>
    public void SpawnExplosion()
    {
        GameObject explode = ObjectPooler.Get.GetPooledObject("SpiderExplode");


        //GameObject explode = Instantiate(PrefabsHelper.Get.SpiderExplode);
        explode.transform.position = transform.position;
        explode.gameObject.SetActive(true);
        explode.GetComponent<Explosion>().Explode();
    }

    public new void Update()
    {
        isCloseToPlayer = true;
        base.Update();
    }
}
