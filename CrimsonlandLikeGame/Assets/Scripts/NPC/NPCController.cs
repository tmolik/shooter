﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{

    public NavMeshAgent NavAgent;

    public string NPCName;

    public ENPCBehaviour CurrentBehaviour;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void MoveToPosition(Vector3 position)
    {
        NavAgent.SetDestination(position);
        CurrentBehaviour = ENPCBehaviour.Moving;
    }


    public void MoveToPosition(Transform destinationTransform)
    {
        NavAgent.SetDestination(destinationTransform.position);
        CurrentBehaviour = ENPCBehaviour.Moving;
    }

    public void ShowDialogue(string context)
    {
        UiDialogueBox dialogueBox = Instantiate(PrefabsHelper.Get.UIDialogueBoxPrefab).GetComponent<UiDialogueBox>();
        dialogueBox.transform.SetParent(UiGameCanvas.Get.transform.Find("Dialogues"));
        dialogueBox.Show(context, transform, Vector3.zero, 3);
    }

}
