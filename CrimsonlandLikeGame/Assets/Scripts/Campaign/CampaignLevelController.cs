﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for levels 
/// </summary>
public class CampaignLevelController : MonoBehaviour
{

    public List<Objective> Objectives = new List<Objective>();

    public Objective CurrentObjective; 

    public void Awake()
    {
        OnLevelStarted();

    }

    public void LevelEnded()
    {
        OnLevelEnded();
    }

    public void StartNewObjective(Objective objective)
    {
        CurrentObjective = objective;
        UiObjectivesController.Get.ShowObjective(objective);
    }

    public void ObjectiveCompleted(string name)
    {
        if (CurrentObjective == null)
            return;

        if(CurrentObjective.Name == name)
        {
            Debug.Log("zakonczylem objective " + name);
            CurrentObjective.Finished = true;

            int index = Objectives.IndexOf(CurrentObjective);
            if(index == Objectives.Count-1)
            {
                LevelEnded();
            }
            else
            {
                StartNewObjective(Objectives[index + 1]);
            }
        }
    }

    public virtual void OnLevelStarted()
    {
        
    }

    public virtual void OnLevelEnded()
    {

    }
}
