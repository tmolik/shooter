﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Campaign_Level1 : CampaignLevelController
{
    public PlayableDirector IntroTimelineDirector;

    public Transform PlayerSpawnPoint;

    public override void OnLevelStarted()
    {
        base.OnLevelStarted();
        PlayerController.Get.WeaponsController.CurrentWeapon.AmmoCount = 6;
        PlayerController.Get.WeaponsController.CurrentWeapon.CurrentMagazineAmmoCount = 6;

        IntroTimelineDirector.stopped += OnStartTimelineStopped;
        if (Objectives.Count > 0)
        {
            StartNewObjective(Objectives[0]);
        }
    }

    public void OnReachedFirstDestination()
    {
        ObjectiveCompleted("Bridge post");
        StartCoroutine(PlayStartTimelineAfterTime());
    }

    private IEnumerator PlayStartTimelineAfterTime()
    {
        yield return new WaitForSeconds(0.5f);
        GameEventsManager.Get.TimelinePlaying = true;
        IntroTimelineDirector.Play();
    }

    public void OnStartTimelineStopped(PlayableDirector aDirector)
    {
        if (aDirector == IntroTimelineDirector)
        {
           
            GameEventsManager.Get.TimelinePlaying = false;

        }
    }
}
