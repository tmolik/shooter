﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Shotgun : WeaponController
{

    public override void CreateBullets(Vector3 targetPosition)
    {
        //Debug.Log("Ebe");

        Vector3 dir1 = targetPosition.normalized;

        Vector3 mainDirection = targetPosition;

        Vector3 secondDirection =/* mainDirection+ */ transform.position + transform.forward * 5f + transform.right * 1f;
        Vector3 thirdDirection = /*mainDirection+*/ transform.position + transform.forward * 5f + transform.right * -1f;

        Vector3 fourthDirection =/* mainDirection+ */ transform.position + transform.forward * 5f + transform.right * 3f;
        Vector3 fifthDirection = /*mainDirection+*/ transform.position + transform.forward * 5f + transform.right * -3f;


        SpawnBullet(mainDirection);
        SpawnBullet(secondDirection);
        SpawnBullet(thirdDirection);
        SpawnBullet(fourthDirection);
        SpawnBullet(fifthDirection);
    }
}
