﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Flamethrower : WeaponController
{

    public ParticleSystem FlamesParticleSystem;

    public AudioSource FlamethrowerAudioSource;

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (PlayerController.Get.Parameters.IsAlive && PlayerController.Get.HoldingFire && !Reloading && AmmoCount > 0)
        {
            FlamesParticleSystem.transform.position = BulletCreateTransform.position;
            FlamesParticleSystem.transform.rotation = Quaternion.LookRotation(PlayerController.Get.TargetShootPosition - PlayerController.Get.transform.position);

            //if (!PlayerController.Get.IsWalking)
            //    FlamesParticleSystem.transform.localRotation = Quaternion.Euler(0, FlamesParticleSystem.transform.localRotation.y, FlamesParticleSystem.transform.localRotation.z);

            if (!FlamesParticleSystem.isPlaying)
                FlamesParticleSystem.Play();

            if (!FlamethrowerAudioSource.isPlaying)
            {
                FlamethrowerAudioSource.Play();
            }

            CurrentMagazineAmmoCount -= 2 * Time.deltaTime;
            if (!GameEventsManager.Get.InfiniteAmmo)
                AmmoCount -= 2 * Time.deltaTime;

            /* If magazine is empty, dont shoot, but if there is ammo at all, start reloading */
            if (CurrentMagazineAmmoCount <= 0)
            {
                if (AmmoCount > 0)
                    StartReloading();
                return;
            }
        }
        else
        {
            if (FlamesParticleSystem.isPlaying)
                FlamesParticleSystem.Stop();

            if (FlamethrowerAudioSource.isPlaying)
            {
                FlamethrowerAudioSource.Stop();
            }
        }
    }

    public void OnDisable()
    {
        if (FlamesParticleSystem.isPlaying)
            FlamesParticleSystem.Stop();

        if (FlamethrowerAudioSource.isPlaying)
        {
            FlamethrowerAudioSource.Stop();
        }
    }

    public override void CreateBullets(Vector3 targetPosition)
    {
    }
}
