﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamethrowerFireController : MonoBehaviour
{

    public void OnParticleCollision(GameObject other)
    {
        HittableObject hittable = other.GetComponent<HittableObject>();
        if (hittable != null && hittable.gameObject.name!="Player")
        {
            hittable.SetOnFire();
            hittable.OnHit(0.5f);
        }
    }
}
