﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for bullet that bounces to another enemy when hits enemy
/// </summary>
public class Bullet_Continous : BulletController
{

    private int Hits = 0;

    public new void OnEnable()
    {
        base.OnEnable();
        Hits = 0;
    }

    public override void HittedSomething(HittableObject hittable, Vector3 hitPoint)
    {
        hittable.OnHit(GetDamageWithPerksIncrease());

        EnemyController closestEnemy = null;
        EnemyController hittedEnemy = hittable.GetComponent<EnemyController>();
        if (hittedEnemy != null)
        {
            float closestDistance = 10000;
            Hits++;
            if (Hits <= 5)
            {
                foreach (EnemyController enemy in LevelInfo.Get.Enemies)
                {
                    if (enemy == hittedEnemy)
                        continue;

                    float dist = Vector3.Distance(hittable.transform.position, enemy.transform.position);
                    if (dist < closestDistance && dist < 15)
                    {
                        closestDistance = dist;
                        closestEnemy = enemy;
                    }
                }

                if (closestEnemy != null)
                    Direction = (closestEnemy.transform.position - transform.position).normalized;
                else
                    gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
        else
        {
            gameObject.SetActive(false);
        }



    }
}
