﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for sniper bullet. It's not destroyed when it hits something
/// </summary>
public class Bullet_Sniper : BulletController
{
    /// <summary> Counter for enemies killed with one shot </summary>
    public int KilledCount = 0;

    public new void OnEnable()
    {
        base.OnEnable();
        KilledCount = 0;
    }

    public override void HittedSomething(HittableObject hittable, Vector3 hitPoint)
    {
        bool isHittableAlive = hittable.OnHit(GetDamageWithPerksIncrease());
        if (!isHittableAlive)
        {
            KilledCount++;
            AchievementsController.EnemiesKilledWithSniperShot(KilledCount);
        }
    }
}
