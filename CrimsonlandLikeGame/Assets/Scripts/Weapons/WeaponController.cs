﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base weapon class
/// </summary>
[System.Serializable]
public class WeaponController : MonoBehaviour
{
    public EWeaponType WeaponType;

    public string BulletPoolableTag;

    public GameObject MuzzleFlashPrefab;

    /// <summary> Instatiated bullet start position is taken from this transform</summary>
    public Transform BulletCreateTransform;

    /// <summary> How much time does it take to reload this gun </summary>
    public float ReloadTime;

    /// <summary> How much time have to pass after shooting another bullet </summary>
    public float TimeBetweenShots;

    /// <summary> Time of last shot </summary>
    private float lastShotTime;

    /// <summary> How many bullets can this magazine keep </summary>
    public int MagazineCapacity;

    /// <summary> How many ammo does player have for this weapon</summary>
    public float AmmoCount;

    /// <summary> How many ammo does player have left in magazine </summary>
    public float CurrentMagazineAmmoCount;

    /// <summary> How much damage is dealt by every bullet </summary>
    public float DamagePerHit;

    /// <summary> Is player currently reloading gun </summary>
    public bool Reloading = false;

    /// <summary> How many time passed since start of reloading </summary>
    private float helperReloadingTimer = 0;

    public Vector3 RotationWhenMoving;
    public Vector3 RotationWhenStanding;

    public Vector3 PositionWhenMoving;
    public Vector3 PositionWhenStanding;

    public float ScreenShakeAmount;

    // Update is called once per frame
    public virtual void Update()
    {
        if (Reloading)
        {
            helperReloadingTimer += Time.deltaTime;
            if (helperReloadingTimer >= GetReloadTime())
            {
                Reloading = false;
                FinishReloading();
            }
        }

        transform.localRotation = Quaternion.Euler(PlayerController.Get.IsWalking ? RotationWhenMoving : RotationWhenStanding);
        transform.localPosition = Vector3.Lerp(transform.localPosition, PlayerController.Get.IsWalking ? PositionWhenMoving : PositionWhenStanding, Time.deltaTime * 10);
    }

    public void Shot(Vector3 clickedPosition)
    {
        /* If weapons is being reloaded, dont shoot */
        if (Reloading)
            return;

        /* If there werent enough time since last shot, dont shoot */
        if (Time.time - lastShotTime < TimeBetweenShots)
            return;

        /* If magazine is empty, dont shoot, but if there is ammo at all, start reloading */
        if (CurrentMagazineAmmoCount == 0)
        {
            if (AmmoCount > 0)
                StartReloading();
            return;
        }

        lastShotTime = Time.time;

        CreateBullets(clickedPosition);
        CreateScreenShake();
        PlayerAudioController.Get.PlayShotSound(WeaponType);

    }

    public virtual void CreateBullets(Vector3 targetPosition)
    {
        SpawnBullet(targetPosition);
    }

    private void CreateScreenShake()
    {
        if (ScreenShakeAmount > 0)
        {
            if (CameraController.Get.shake < ScreenShakeAmount)
                CameraController.Get.shake = ScreenShakeAmount;
        }
    }

    public void SublimeAmmo()
    {
        if(LevelInfo.Get.GameMode == EGameMode.Campaign)
        {
            AmmoCount--;
        }
        else if (WeaponType != EWeaponType.Pistol)
        {
            if (!GameEventsManager.Get.InfiniteAmmo)
                AmmoCount--;
        }
        CurrentMagazineAmmoCount--;

        if (CurrentMagazineAmmoCount == 0 && AmmoCount > 0)
            StartReloading();

        if (AmmoCount == 0 && WeaponType != EWeaponType.Pistol && WeaponType != EWeaponType.HeavyMachineGun)
        {
            PlayerController.Get.WeaponsController.ChangeWeapon(EWeaponType.Pistol, 999);
        }
    }

    /// <summary>
    /// Starts reloading, sets helper values
    /// </summary>
    public void StartReloading()
    {
        helperReloadingTimer = 0;
        Reloading = true;
        if (WeaponType == EWeaponType.Pistol || WeaponType == EWeaponType.Uzi)
        {
            PlayerAudioController.Get.PlayReload();
        }
    }

    /// <summary>
    /// Finishes reloading, sets magazine ammo as full as it can
    /// </summary>
    public void FinishReloading()
    {
        if (AmmoCount > 0)
        {
            CurrentMagazineAmmoCount = Mathf.Min(MagazineCapacity, AmmoCount);
        }
    }

    /// <summary>
    /// Spawns bullet and sets its direction and damage values
    /// </summary>
    /// <param name="targetPosition"></param>
    public void SpawnBullet(Vector3 targetPosition)
    {
        BulletController bullet = ObjectPooler.Get.GetPooledObject(BulletPoolableTag).GetComponent<BulletController>();

        if (bullet == null)
            return;

        bullet.transform.position = BulletCreateTransform.transform.position;
        bullet.transform.rotation = BulletCreateTransform.transform.rotation;
        //bullet.Direction = (targetPosition - PlayerController.Get.transform.position).normalized;
        Vector3 direction = (targetPosition - BulletCreateTransform.transform.position).normalized;

        bullet.Direction = new Vector3(direction.x, 0, direction.z);

        bullet.Damage = DamagePerHit;
        bullet.gameObject.SetActive(true);
        SublimeAmmo();

        GameObject muzzle = Instantiate(MuzzleFlashPrefab, BulletCreateTransform.transform.position, Quaternion.Euler(BulletCreateTransform.transform.rotation.eulerAngles + new Vector3(0, -90, 0)));
        muzzle.transform.SetParent(transform);

        // SpawnShell();
    }

    public void SpawnShell()
    {
        GameObject shell = ObjectPooler.Get.GetPooledObject("Shell");
        shell.transform.position = BulletCreateTransform.transform.position - BulletCreateTransform.forward * 0.2f + Vector3.up * 0.5f;
        shell.transform.rotation = Quaternion.Euler(Vector3.zero);
        shell.gameObject.SetActive(true);
        shell.GetComponent<Rigidbody>().AddForce(Vector3.up * 30 + Random.insideUnitSphere * 10);
    }

    public float GetReloadTime()
    {
        float timeDecreasePercent = PerksController.Perks[EPerkType.ReloadTimeDecrease].CurrentValue;

        float decreasedValue = (timeDecreasePercent / 100) * ReloadTime;

        return ReloadTime - decreasedValue;
    }

}
