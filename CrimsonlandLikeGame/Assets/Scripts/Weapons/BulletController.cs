﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base bullet class
/// </summary>
public class BulletController : MonoBehaviour
{
    /// <summary>Direction that bullet is going</summary>
    internal Vector3 Direction;

    /// <summary>How many damage will this bullet apply to hittable object it hits</summary>
    internal float Damage;

    /// <summary>How fast does this bullet move</summary>
    public float Speed;

    /// <summary>Helper timer to destroy bullet after some time</summary>
    private float helperTimer;

    private Vector3 previousPosition;


    public void OnEnable()
    {
        helperTimer = 0;
        previousPosition = transform.position;
    }

    public void OnDisable()
    {
        /*Reset trail renderer*/
        GetComponent<TrailRenderer>().Clear();
    }

    // Update is called once per frame
    void Update()
    {
        previousPosition = transform.position;

        /* Moving forward */
        if (Direction != Vector3.zero)
        {
            transform.position += Direction * Speed * Time.deltaTime;
            transform.rotation = Quaternion.LookRotation((transform.position + Direction * 4) - transform.position);
        }

        CheckIfHittedSomething();


        ///* Destroing it after 5 seconds */
        helperTimer += Time.deltaTime;
        if (helperTimer >= 9)
            gameObject.SetActive(false);

    }

    /// <summary>
    /// Checks if bullet hitted something and invokes Hitted function
    /// </summary>
    public void CheckIfHittedSomething()
    {
        RaycastHit[] hits = Physics.RaycastAll(new Ray(previousPosition, (transform.position - previousPosition).normalized), (transform.position - previousPosition).magnitude);

        foreach (RaycastHit hit in hits)
        {
            HittableObject hittable = hit.collider.GetComponent<HittableObject>();
            if (hittable)
            {
                HittedSomething(hittable, hit.point);
            }
        }

    }

    /// <summary>
    /// Invoked when bullet hits some hittable object
    /// </summary>
    /// <param name="hittable"></param>
    /// <param name="hitPoint"></param>
    public virtual void HittedSomething(HittableObject hittable, Vector3 hitPoint)
    {
        hittable.OnHit(GetDamageWithPerksIncrease());

        gameObject.SetActive(false);
    }

    /// <summary>
    /// Returns damage calculated with perk increases
    /// </summary>
    /// <returns></returns>
    public float GetDamageWithPerksIncrease()
    {
        float damageIncreasePercent = PerksController.Perks[EPerkType.DamageIncrease].CurrentValue;

        float increasedValue = (damageIncreasePercent / 100) * Damage;

        return Damage + increasedValue;
    }
}
