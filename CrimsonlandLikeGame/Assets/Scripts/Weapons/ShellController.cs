﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellController : MonoBehaviour
{

    private float helperTimer = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        helperTimer += Time.deltaTime;
        if (helperTimer >= 5)
        {
            helperTimer = 0;
            gameObject.SetActive(false);
        }
    }
}
