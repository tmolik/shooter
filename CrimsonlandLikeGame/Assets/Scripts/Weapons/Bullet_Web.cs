﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Web : BulletController
{

    public override void HittedSomething(HittableObject hittable, Vector3 hitPoint)
    {
        PlayerParameters playerParameters = hittable.GetComponent<PlayerParameters>();

        if (playerParameters != null)
        {
            if(Random.Range(0,100) > 70)
            {
                playerParameters.Stun(1);
            }
        }

        base.HittedSomething(hittable, hitPoint);
    }
}
