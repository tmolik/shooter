﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for rocket launcher bullet that explodes when hits something
/// </summary>
public class Bullet_Grenade : BulletController
{
    private bool explodedOnce = false;

    public override void HittedSomething(HittableObject hittable, Vector3 hitPoint)
    {
        if (!explodedOnce)
        {
            GameObject explode = ObjectPooler.Get.GetPooledObject("Explode");
            explode.transform.position = hitPoint;
            explode.gameObject.SetActive(true);
            explode.GetComponent<Explosion>().Explode();
            explodedOnce = true;
        }

        gameObject.SetActive(false);
    }

    public new void OnEnable()
    {
        base.OnEnable();
        explodedOnce = false;
    }
}
