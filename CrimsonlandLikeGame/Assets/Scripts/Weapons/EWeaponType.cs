﻿public enum EWeaponType {

	Pistol,
    Uzi,
    Shotgun,
    Sniper,
    RocketLauncher,
    Flamethrower,
    HeavyMachineGun
}
