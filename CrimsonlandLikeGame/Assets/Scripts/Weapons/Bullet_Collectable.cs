﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Collectable : BulletController
{
    public override void HittedSomething(HittableObject hittable, Vector3 hitPoint)
    {
        if (hittable.gameObject.name == "Player")
            return;

        base.HittedSomething(hittable, hitPoint);
    }

}
