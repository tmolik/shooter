﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perk
{

    public EPerkType PerkType;

    public int Level;

    public float ValueIncreasedPerLevel;

    public float CurrentValue;

    public string Title;

    public string Description;

    public Perk(EPerkType perkType, int level, float valueIncreasedPerLevel, float currentValue, string title, string description)
    {
        PerkType = perkType;
        Level = level;
        ValueIncreasedPerLevel = valueIncreasedPerLevel;
        CurrentValue = currentValue;
        Title = title;
        Description = description;
    }

    public void LevelUp()
    {
        Level++;
        CurrentValue = CurrentValue + ValueIncreasedPerLevel;
        OnLevelChanged();
    }

    public virtual void OnLevelChanged()
    {

    }
}
