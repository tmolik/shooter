﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Get { get; private set; }

    public CameraController()
    {
        Get = this;
    }


    public float shake = 0;
    public float shakeAmount = 1f;
    public float decreaseFactor = 1.0f;

    public float CameraHeight;

    public Camera CameraComponent;

    // Use this for initialization
    void Start()
    {
        GameObject.DontDestroyOnLoad(this);
        CameraComponent = GetComponent<Camera>();
        CameraHeight = 22f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!MainController.LevelLoaded)
            return;
    
        Vector3 shakeBonus = Vector3.zero;
        if (shake > 0)
        {
            shakeBonus = Random.insideUnitSphere * shakeAmount;
            shakeBonus = new Vector3(shakeBonus.x, 0, shakeBonus.z);
            shake -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shake = 0.0f;
        }
        Vector3 targetOffset = (PlayerController.Get.TargetShootPosition - PlayerController.Get.transform.position).normalized*0.5f;
        targetOffset = new Vector3(targetOffset.x, 0, targetOffset.z);
        transform.position = Vector3.Lerp(transform.position, PlayerController.Get.transform.position + Vector3.up * CameraHeight + shakeBonus + targetOffset, Time.deltaTime * 2);
    }
}
