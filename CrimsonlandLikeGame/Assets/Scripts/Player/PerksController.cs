﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class PerksController
{
    public static  Dictionary<EPerkType, Perk> Perks = new Dictionary<EPerkType, Perk>();

    public static void ResetAllPerks()
    {
        if (Perks == null)
            Perks = new Dictionary<EPerkType, Perk>();
        Perks.Clear();
        Perks.Add(EPerkType.DamageIncrease, new Perk(EPerkType.DamageIncrease, 0, 5, 0,"Brute force!","Damage increased"));
        Perks.Add(EPerkType.MovementSpeedIncrease, new Perk(EPerkType.MovementSpeedIncrease, 0, 2, 0,"Runner!", "Movement speed increased"));
        Perks.Add(EPerkType.ReloadTimeDecrease, new Perk(EPerkType.ReloadTimeDecrease, 0, 5, 0,"Marksman!", "Reload time decreased"));
        Perks.Add(EPerkType.CollectablesSpawnTime, new Perk(EPerkType.CollectablesSpawnTime, 0, 5, 0,"Collector!", "Collectables spawn time decreased"));
        Perks.Add(EPerkType.MaxHealthIncrease, new Perk(EPerkType.MaxHealthIncrease, 0, 5, 0, "Tank!", "Max health increased"));
        Perks.Add(EPerkType.MaxEnergyIncrease, new Perk(EPerkType.MaxEnergyIncrease, 0, 5, 0, "Energetic!", "Max energy increased"));
        Perks.Add(EPerkType.CollectMoreAmmo, new Perk(EPerkType.CollectMoreAmmo, 0, 5, 0, "Give me more!", "Collected ammo count increased"));
    }

    public static  void LevelUpPerk(EPerkType perkType)
    {
        if (Perks != null)
        {
            Perks[perkType].LevelUp();
        }
    }

    public static List<Perk> GetThreeRandomPerks()
    {
        List<Perk> perks = new List<Perk>();

        while (perks.Count < 3)
        {
            EPerkType perkType = (EPerkType)UnityEngine.Random.Range(0, Enum.GetValues(typeof(EPerkType)).Length);

            if (perks.Count(p => p.PerkType == perkType) > 0)
                continue;

            perks.Add(Perks[perkType]);
        }

        return perks;
    }

}
