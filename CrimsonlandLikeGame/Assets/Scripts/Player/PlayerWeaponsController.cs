﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponsController : MonoBehaviour
{
    public List<WeaponReferenceHelper> PlayerWeapons = new List<WeaponReferenceHelper>();

    public WeaponController CurrentWeapon;



    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            ChangeWeapon(EWeaponType.Pistol);

        if (Input.GetKeyDown(KeyCode.Alpha2))
            ChangeWeapon(EWeaponType.Uzi);

        if (Input.GetKeyDown(KeyCode.Alpha3))
            ChangeWeapon(EWeaponType.Shotgun);

        if (Input.GetKeyDown(KeyCode.Alpha4))
            ChangeWeapon(EWeaponType.Sniper);

        if (Input.GetKeyDown(KeyCode.Alpha5))
            ChangeWeapon(EWeaponType.RocketLauncher);

        if (Input.GetKeyDown(KeyCode.Alpha6))
            ChangeWeapon(EWeaponType.Flamethrower);


        
    }


    public void Shot(Vector3 targetShotPosition)
    {
        if (CurrentWeapon != null)
        {
            CurrentWeapon.Shot(targetShotPosition);
        }
    }

    public void ChangeWeapon(EWeaponType weaponType, int ammoCount, bool force = false)
    {
        if (CurrentWeapon.WeaponType == weaponType)
        {
            CurrentWeapon.AmmoCount = Mathf.Min(999, CurrentWeapon.AmmoCount + ammoCount);
            return;
        }

        if (MainController.Get.CurrentLevelPreset != null && !MainController.Get.CurrentLevelPreset.CanChangeWeapon && !force)
            return;

        CurrentWeapon.gameObject.SetActive(false);

        WeaponController newWeapon = GetPlayerWeapon(weaponType).GetComponent<WeaponController>();
        newWeapon.gameObject.SetActive(true);
        CurrentWeapon = newWeapon;
        CurrentWeapon.AmmoCount = ammoCount;
        CurrentWeapon.FinishReloading();

        CrosshairController.Get.SwitchCrosshair(weaponType);
    }

    public void ChangeWeapon(EWeaponType weaponType,bool force=false)
    {

        if (MainController.Get.CurrentLevelPreset != null && !MainController.Get.CurrentLevelPreset.CanChangeWeapon && !force)
            return;

        WeaponController newWeapon = GetPlayerWeapon(weaponType).GetComponent<WeaponController>();
        if (newWeapon.AmmoCount>0)
        {
            CurrentWeapon.gameObject.SetActive(false);
            newWeapon.gameObject.SetActive(true);
            CurrentWeapon = newWeapon;
            CrosshairController.Get.SwitchCrosshair(weaponType);
        }
    }

    public void SetCurrentWeaponForce(WeaponController weapon)
    {
        CurrentWeapon.gameObject.SetActive(false);
        
        weapon.gameObject.SetActive(true);
        CurrentWeapon = weapon;
        CurrentWeapon.FinishReloading();

        CrosshairController.Get.SwitchCrosshair(weapon.WeaponType);
    }

    private GameObject GetPlayerWeapon(EWeaponType weaponType)
    {
        foreach (WeaponReferenceHelper weapon in PlayerWeapons)
        {
            if (weapon.WeaponType == weaponType)
                return weapon.WeaponGameobject;
        }
        return null;
    }


    public void Reset()
    {
        GetPlayerWeapon(EWeaponType.RocketLauncher).GetComponent<WeaponController>().AmmoCount = 0;
        GetPlayerWeapon(EWeaponType.RocketLauncher).GetComponent<WeaponController>().CurrentMagazineAmmoCount = 0;

        GetPlayerWeapon(EWeaponType.Uzi).GetComponent<WeaponController>().AmmoCount = 0;
        GetPlayerWeapon(EWeaponType.Uzi).GetComponent<WeaponController>().CurrentMagazineAmmoCount = 0;

        GetPlayerWeapon(EWeaponType.Shotgun).GetComponent<WeaponController>().AmmoCount = 0;
        GetPlayerWeapon(EWeaponType.Shotgun).GetComponent<WeaponController>().CurrentMagazineAmmoCount = 0;

        GetPlayerWeapon(EWeaponType.Sniper).GetComponent<WeaponController>().AmmoCount = 0;
        GetPlayerWeapon(EWeaponType.Sniper).GetComponent<WeaponController>().CurrentMagazineAmmoCount = 0;

        ChangeWeapon(EWeaponType.Pistol);
    }

}
