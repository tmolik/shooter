﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Main player controller
/// </summary>
public class PlayerController : MonoBehaviour
{

    public static PlayerController Get { get; private set; }

    public PlayerController()
    {
        Get = this;
    }

    public Animator PlayerAnimator;

    public PlayerParameters Parameters;

    public PlayerWeaponsController WeaponsController;

    /// <summary> Position that player is going to </summary>
    public Vector3 TargetMovePosition;

    /// <summary>Position clicked to shoot and rotate to</summary>
    public Vector3 TargetShootPosition;

    /// <summary>Helper bool value for situations when player tapped once on screen. It indicates if player should shoot after he rotate to target rotation </summary>
    private bool wantsToShoot = false;

    /// <summary>helper bool value that indicates if player is holding touch input to shoot continuosly</summary>
    internal bool HoldingFire = false;

    public bool IsWalking = false;

    public Transform SpineTransform;

    public Transform FlashlightTransform;

    private Vector3 MoveDirection;

    public ParticleSystem ShieldParticle;

    public EPlayerMode CurrentPlayerMode = EPlayerMode.Moving;

    public InteractableObject CurrentlyUsedInteractable;

    public void Start()
    {
        Parameters = gameObject.AddComponent<PlayerParameters>();
        WeaponsController = transform.Find("Weapons").GetComponent<PlayerWeaponsController>();
        GameObject.DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (!MainController.LevelLoaded)
            return;

        if (!Parameters.IsAlive)
            return;

        /* Fixing bad animation rotation*/
        //PlayerAnimator.transform.localRotation = Quaternion.Euler(IsWalking ? Vector3.zero : new Vector3(0, -15, 0));


        if (TargetShootPosition != Vector3.zero)
        {
            if (HoldingFire)
                wantsToShoot = true;

            if (wantsToShoot)
            {
                WeaponsController.Shot(TargetShootPosition);
                wantsToShoot = false;
            }
        }

        if (IsWalking)
            PlayerAnimator.SetFloat("WalkSpeed", Parameters.GetSpeed());


        PCInputsUpdate();
    }

    public LayerMask RaycastLayerMask;

    /// <summary>
    /// Checking player input, setting target shoot position, checking if he wants to shot, changing animations
    /// </summary>
    public void PCInputsUpdate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, 1000))
        {
            TargetShootPosition = new Vector3(raycastHit.point.x, transform.position.y, raycastHit.point.z);
        }

        if (UIMainMenuController.Get.CanShoot() && !UiAchievementsList.Showing && !GameEventsManager.Get.TimelinePlaying)
        {
            if (Input.GetMouseButton(0))
            {
                HoldingFire = true;
            }

            if (Input.GetMouseButtonUp(0))
            {
                HoldingFire = false;
            }
        }

        if (Parameters.Stunned)
        {
            PlayerAnimator.SetFloat("WalkSpeed", 0);

            //PlayerAnimator.CrossFade("Idle", 0.1f);
            return;
        }

        if (CurrentPlayerMode == EPlayerMode.Moving)
        {

            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            Vector3 movingDirection = transform.position + new Vector3(horizontal, 0, vertical) * 4;
            MoveDirection = movingDirection;

            if (!Parameters.Stunned && !GameEventsManager.Get.TimelinePlaying)
                transform.position += new Vector3(horizontal, 0, vertical) * (Parameters.GetSpeed()) * 4 * Time.deltaTime;

            if (horizontal > 0f || horizontal < 0 || vertical > 0 || vertical < 0)
            {
                if (!IsWalking)
                {
                    PlayerAnimator.SetFloat("WalkSpeed", Parameters.GetSpeed());

                    PlayerAnimator.CrossFade("Walk", 0.3f);
                    IsWalking = true;
                }
            }
            else
            {
                if (IsWalking)
                {
                    PlayerAnimator.SetFloat("WalkSpeed", 0);
                    //PlayerAnimator.CrossFade("Idle", 0.1f);
                    IsWalking = false;
                }
            }
        }
        else
        {
            PlayerAnimator.SetFloat("WalkSpeed", 0);
        }
    }


    public void LateUpdate()
    {
        if (Parameters.IsAlive)
        {
            if (CurrentPlayerMode == EPlayerMode.Moving)
            {

                /* Whole player is rotated in the direction he is walking, then his upper half is rotated in shooting direction */

                if (IsWalking)
                {
                    transform.rotation = Quaternion.LookRotation(MoveDirection - transform.position);
                }
                else
                {
                    //transform.rotation = Quaternion.LookRotation(MoveDirection - transform.position);

                    //transform.rotation = Quaternion.LookRotation(TargetShootPosition - transform.position);
                }


                Quaternion spineRotation = Quaternion.LookRotation(TargetShootPosition - transform.position);
                spineRotation = Quaternion.Euler(spineRotation.eulerAngles.x, spineRotation.eulerAngles.y + (IsWalking ? 35 : 45), spineRotation.eulerAngles.z);

                SpineTransform.rotation = spineRotation;
                FlashlightTransform.rotation = Quaternion.LookRotation(TargetShootPosition - transform.position);
            }
            else
            {
                if (CurrentlyUsedInteractable != null)
                {
                    Vector3 lookDirection = CurrentlyUsedInteractable.transform.position - transform.position;
                    lookDirection.y = 0;
                    transform.rotation = Quaternion.LookRotation(lookDirection);
                }
            }
        }

    }

    /// <summary>
    /// Resets player parameters, perks, werapons and animation
    /// </summary>
    public void ResetPlayer()
    {
        Destroy(Parameters);
        Parameters = gameObject.AddComponent<PlayerParameters>();
        WeaponsController.Reset();
        PlayerAnimator.CrossFade("Idle", 0.1f);
        IsWalking = false;
        PerksController.ResetAllPerks();
    }



    /// <summary>
    /// Sets shield partice emission rate to 10
    /// </summary>
    public void EnableShield()
    {
        var emission = ShieldParticle.emission;
        var rate = emission.rateOverTime;
        rate.constantMax = 10f;
        emission.rateOverTime = rate;
    }

    /// <summary>
    /// Lowers shield particle emission rate to 0
    /// </summary>
    public void DisableShield()
    {
        var emission = ShieldParticle.emission;
        var rate = emission.rateOverTime;
        rate.constantMax = 0f;
        emission.rateOverTime = rate;
    }
}
