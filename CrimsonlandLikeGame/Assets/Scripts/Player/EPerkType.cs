﻿public enum EPerkType
{
    DamageIncrease,
    MovementSpeedIncrease,
    ReloadTimeDecrease,
    CollectablesSpawnTime,
    MaxHealthIncrease,
    MaxEnergyIncrease,
    CollectMoreAmmo
}
