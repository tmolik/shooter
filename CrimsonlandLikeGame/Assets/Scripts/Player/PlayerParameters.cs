﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class with basic player parameters like health and armor
/// </summary>
[System.Serializable]
public class PlayerParameters : HittableObject
{
    /// <summary>Player current energy</summary>
    public float Energy;

    /// <summary>Current armor value</summary>
    public float Armor;

    /// <summary>Player speed</summary>
    public float Speed;

    /// <summary>Speed bonus(from consumable speed pill)</summary>
    public float SpeedBonus;

    public int Level = 0;

    public int Experience = 0;

    public int ExperienceNeededForNextLevel = -1;

    public GameObject CocoonGameObject;
    public bool Stunned;
    public float StunTime;
    private float stunHelperTimer;

    public float SprintBonus;

    /// <summary>helper value to remove speed bonus after this time</summary>
    private float RemoveSpeedBonusAfter;
    private float helperTimer;

    public bool HasShield;
    private float shieldTimer;

    // Use this for initialization
    new void Start()
    {
        base.Start();
        Health = 100;
        Armor = 0;
        Speed = 1;
        SpeedBonus = 0;
        Energy = 100;
        CocoonGameObject = transform.Find("Cocoon").gameObject;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();

        helperTimer += Time.deltaTime;
        if (helperTimer >= RemoveSpeedBonusAfter)
        {
            SpeedBonus = 0;
        }

        if (Stunned)
        {
            stunHelperTimer += Time.deltaTime;
            if (stunHelperTimer >= StunTime)
            {
                Stunned = false;
                CocoonGameObject.SetActive(false);

                if (PlayerController.Get.IsWalking)
                    PlayerController.Get.PlayerAnimator.CrossFade("Walk", 0.3f);
            }
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Energy > 0)
            {
                SprintBonus = 0.5f;
            }
            else
            {
                SprintBonus = 0;
            }

            Energy -= 30 * Time.deltaTime;
            if (Energy < 0)
                Energy = 0;
        }
        else
        {
            SprintBonus = 0;
            Energy += 5 * Time.deltaTime;
            if (Energy > GetMaxEnergy())
                Energy = GetMaxEnergy();
        }

        if (HasShield)
        {
            shieldTimer += Time.deltaTime;

            if (shieldTimer >= 10)
            {
                HasShield = false;
                PlayerController.Get.DisableShield();

            }
        }

        if (Input.GetKeyDown(KeyCode.U))
            Stun(4);
    }

    public void SetShield()
    {
        HasShield = true;
        shieldTimer = 0;
        PlayerController.Get.EnableShield();
    }

    public void AddSpeedBonus(float bonus, float time)
    {
        helperTimer = 0;
        SpeedBonus = bonus;
        RemoveSpeedBonusAfter = time;
    }

    public void Stun(float stunTime)
    {
        stunHelperTimer = 0;
        StunTime = stunTime;
        Stunned = true;
        CocoonGameObject.SetActive(true);

    }

    /// <summary>
    /// Invoked when player is attacked by enemy, it goes through armor and then to heallth
    /// </summary>
    /// <param name="damage"></param>
    public void Attacked(float damage)
    {
        if (HasShield)
            return;

        if (Armor > 0)
        {
            Armor -= damage;
            if (Armor > 0)
            {
                return;
            }
            else
            {
                damage = Mathf.Abs(Armor);
            }
        }

        OnHit(damage);
        //Health = Mathf.Max(0, Health - damage);
        //UiVignetteController.Get.GetHit();

    }

    public override void OnZeroHealth()
    {
        if(PlayerController.Get.CurrentPlayerMode == EPlayerMode.UsingObject)
        {
            if (PlayerController.Get.CurrentlyUsedInteractable != null)
                PlayerController.Get.CurrentlyUsedInteractable.StopInteracting();
        }
        PlayerController.Get.PlayerAnimator.CrossFade("Death", 0.3f);
        PlayerController.Get.PlayerAnimator.SetFloat("WalkSpeed", 1);

        MainController.Get.GameOver();
        PlayerAudioController.Get.PlayDeath();

        SurvivalModeController survival = SurvivalModeController.Get;
        if (survival != null)
        {
            AchievementsController.SpentTimeInSurvival((int)survival.SurvivedTime);
        }

    }

    public override bool OnHit(float damage)
    {
        if (HasShield)
            return IsAlive;

        base.OnHit(damage);
        UiVignetteController.Get.GetHit();

        if (damage > 1)
        {
            GameObject bloodSplat = Instantiate(PrefabsHelper.Get.BloodSplatPrefab);
            bloodSplat.transform.position = new Vector3(transform.position.x, 0.01f, transform.position.z);
            bloodSplat.transform.rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
            bloodSplat.transform.SetParent(LevelInfo.Get.transform);
            PlayerAudioController.Get.PlayGetHit();
        }
        else
        {
            if (Random.Range(0, 100) > 95)
                PlayerAudioController.Get.PlayGetHit();

        }
        return IsAlive;
    }

    public float GetSpeed()
    {
        float baseSpeedValue = Speed + SpeedBonus + SprintBonus;
        float speedIncreaseValue = (PerksController.Perks[EPerkType.MovementSpeedIncrease].CurrentValue / 100) * baseSpeedValue;

        return baseSpeedValue + speedIncreaseValue;
    }

    public float GetMaxHealth()
    {
        float healthIncreased = (PerksController.Perks[EPerkType.MaxHealthIncrease].CurrentValue / 100 * 100);
        return 100 + healthIncreased;
    }

    public float GetMaxEnergy()
    {
        float energyIncrease = (PerksController.Perks[EPerkType.MaxEnergyIncrease].CurrentValue / 100 * 100);
        return 100 + energyIncrease;
    }

    public void GainExp(int exp)
    {
        if (LevelInfo.Get != null && LevelInfo.Get.GameMode == EGameMode.Campaign)
            return;

        if (ExperienceNeededForNextLevel == -1)
            CalculateExpForNextLevel();
        Experience += exp;
        if (Experience > ExperienceNeededForNextLevel)
        {
            Level++;
            CalculateExpForNextLevel();
            UiPerksController.Get.ShowPerks();
        }
    }

    public void CalculateExpForNextLevel()
    {
        ExperienceNeededForNextLevel = (int)Mathf.Pow(10 * (Level + 1), 2) + 50 * (Level + 1) + 50;
    }


    public void NewLevel()
    {

        Health = GetMaxHealth();
        Energy = GetMaxEnergy();
        if (Stunned)
        {
            Stunned = false;
            CocoonGameObject.SetActive(false);

            if (PlayerController.Get.IsWalking)
                PlayerController.Get.PlayerAnimator.CrossFade("Walk", 0.3f);
        }
        if (IsOnFire)
        {
            OnFireTimer = 6;
        }
    }

}
