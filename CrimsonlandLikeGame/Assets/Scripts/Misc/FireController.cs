﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour {

    public HittableObject FireBearer;

    public ParticleSystem FireParticle;

    public void OnStart()
    {
        FireParticle = GetComponent<ParticleSystem>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (!MainController.LevelLoaded && !MainController.IsPreparingLevel)
            return;

        if (FireBearer!=null &&  other.gameObject.name == FireBearer.gameObject.name)
            return;

        if (FireParticle == null)
            FireParticle = GetComponent<ParticleSystem>();
        if (FireParticle.emission.rateOverTime.constantMax == 0)
            return;

        //Debug.Log("Weszło na ten fire " + gameObject.name);

        HittableObject hittable = other.GetComponent<HittableObject>();

        if (hittable != null)
        {
            hittable.SetOnFire();
        }

    }

    public void Update()
    {
        if (FireBearer == null)
            gameObject.SetActive(false);
    }
}
