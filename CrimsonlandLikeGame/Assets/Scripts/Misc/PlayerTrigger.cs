﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTrigger : MonoBehaviour
{
    public UnityEvent OnPlayerEnterTrigger;

    public bool TriggerOnlyOnce = true;

    private bool triggered = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null)
        {
            if (TriggerOnlyOnce && triggered)
                return;
            triggered = true;

            OnPlayerEnterTrigger.Invoke();
        }
    }
}
