﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAllEfect : MonoBehaviour
{

    public int Size;

    private float helperTimer;

    // Update is called once per frame
    void Update()
    {
        helperTimer += Time.deltaTime;
        transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * Size*4, helperTimer / 0.4f);

        if (helperTimer > 0.4f)
            Destroy(gameObject);
    }
}
