﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion_Spider : Explosion
{

    public override void Explode()
    {
        base.Explode();

        AchievementsController.EnemyKilledWithExplosiveSpider(EnemiesKilled - 1);
    }

    
}
