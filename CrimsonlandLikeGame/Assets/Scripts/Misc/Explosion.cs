﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to control explosion logic
/// </summary>
public class Explosion : MonoBehaviour
{

    /// <summary>What range will this explosion affect</summary>
    public float Range;

    /// <summary>Damage taken from explosion, it depends on distance from hittable object to explosion position</summary>
    public float Damage;

    public AudioSource ExplosionAudioSource;

    public int EnemiesKilled;

    private float helperTimer;


    /// <summary>
    /// Goes throught all hittable object and damages them if they are in range
    /// </summary>
    public virtual void Explode()
    {
        EnemiesKilled = 0;
        Vector3 viewportPosition = Camera.main.WorldToViewportPoint(transform.position);
        if (viewportPosition.x > 0 && viewportPosition.x < 1 && viewportPosition.y > 0 && viewportPosition.y < 1)
        {
            CameraController.Get.shake = 0.4f;
        }
        else
        {

        }
        foreach (HittableObject hittable in GameController.Get.Hittables)
        {
            float distance = Vector3.Distance(hittable.transform.position, transform.position);
            if (distance < Range)
            {
                float distanceMultiplier = Mathf.Lerp(1, 0, distance / Range);
                bool isHittableAlive = hittable.OnHit(Damage * distanceMultiplier);
                EnemyController enemy = hittable.GetComponent<EnemyController>();
                if (enemy)
                {
                    if (isHittableAlive)
                        enemy.Push(transform.position);

                    if (Random.Range(0, 100) > 60)
                        enemy.SetOnFire();

                    if (!isHittableAlive)
                        EnemiesKilled++;
                }

                PlayerParameters player = hittable.GetComponent<PlayerParameters>();
                if (player != null && distance < 10)
                    AudioManager.Get.PlayAfterExplosionClip();
            }
        }

        if (ExplosionAudioSource != null)
            ExplosionAudioSource.PlayOneShot(AudioManager.Get.BarrelExplosionClips.GetRandomAudioClip());
    }

    public void Update()
    {
        helperTimer += Time.deltaTime;
        if (helperTimer > 3)
            gameObject.SetActive(false);
    }

    public void OnEnable()
    {
        helperTimer = 0;
        GetComponent<ParticleSystem>().Play();
        foreach (ParticleSystem particle in GetComponentsInChildren<ParticleSystem>())
        {
            particle.Play();
        }
    }
}
