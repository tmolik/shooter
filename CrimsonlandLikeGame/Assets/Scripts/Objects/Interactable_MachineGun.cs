﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable_MachineGun : InteractableObject
{

    public Transform PlayerTransform;

    public Transform RotatingTransform;

    public WeaponController MachineGunWeapon;

    public UnityEvent OnZeroAmmoEvent;
    private bool invokedZeroAmmo = false;

    public Quaternion startingRotation;

    // Use this for initialization
    void Start()
    {
        startingRotation = transform.rotation;
    }

    // Update is called once per frame
    public new void Update()
    {
        base.Update();

        if (!PlayerInteracting)
            return;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;

        Vector3 targetShootPosition = Vector3.zero;

        if (Physics.Raycast(ray, out raycastHit, 1000))
        {
            targetShootPosition = new Vector3(raycastHit.point.x, transform.position.y, raycastHit.point.z);
        }

        //Quaternion targetRotation = Quaternion.LookRotation(targetShootPosition - transform.position);
        //float yClamped = ClampAngle(targetRotation.eulerAngles.y, startingRotation.eulerAngles.y - 65, startingRotation.eulerAngles.y + 65);
        //RotatingTransform.rotation = Quaternion.Euler(targetRotation.eulerAngles.x, yClamped, targetRotation.eulerAngles.z);

        RotatingTransform.rotation = Quaternion.LookRotation(targetShootPosition - transform.position);
        

        PlayerController.Get.transform.position = PlayerTransform.position;

        if(MachineGunWeapon.AmmoCount ==0 && !invokedZeroAmmo)
        {
            invokedZeroAmmo = true;
            OnZeroAmmoEvent.Invoke();
        }

    }

    float ClampAngle(float angle, float from, float to)
    {
        // accepts e.g. -80, 80
        if (angle < 0f) angle = 360 + angle;
        if (angle > 180f) return Mathf.Max(angle, 360 + from);
        return Mathf.Min(angle, to);
    }

    public override void OnStartedInteraction()
    {
        base.OnStartedInteraction();
        PlayerController.Get.CurrentPlayerMode = EPlayerMode.UsingObject;
        PlayerController.Get.transform.position = PlayerTransform.position;
        PlayerController.Get.transform.SetParent(PlayerTransform);
        PlayerController.Get.WeaponsController.SetCurrentWeaponForce(MachineGunWeapon);
        PlayerController.Get.FlashlightTransform.gameObject.SetActive(false);
        
    }

    public override void OnFinishedInteraction()
    {
        base.OnFinishedInteraction();
        PlayerController.Get.CurrentPlayerMode = EPlayerMode.Moving;
        PlayerController.Get.transform.SetParent(null);
        PlayerController.Get.WeaponsController.ChangeWeapon(EWeaponType.Pistol,true);
        PlayerController.Get.FlashlightTransform.gameObject.SetActive(true);
        MachineGunWeapon.gameObject.SetActive(true);
    }
}
