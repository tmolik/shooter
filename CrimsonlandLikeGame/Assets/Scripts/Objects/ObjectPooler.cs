﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler Get { get; set; }

    public ObjectPooler()
    {
        Get = this;
    }

    public List<ObjectToPoolPreset> ObjectsToPool = new List<ObjectToPoolPreset>();

    public List<PoolableObject> PooledObjects = new List<PoolableObject>();


    // Use this for initialization
    void Start()
    {
        foreach (ObjectToPoolPreset objectToPool in ObjectsToPool)
        {
            for (int i = 0; i < objectToPool.AmountToPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(objectToPool.ObjectToPool.gameObject);
                obj.SetActive(false);
                PooledObjects.Add(obj.GetComponent<PoolableObject>());
                obj.transform.SetParent(transform);
            }
        }
    }

    public GameObject GetPooledObject(string tag)
    {
        for (int i = 0; i < PooledObjects.Count; i++)
        {
            if (PooledObjects[i] == null)
            {
                Debug.Log("Cos tu jest nullem wtf dla i " + i);
                continue;
            }
            if (!PooledObjects[i].gameObject.activeInHierarchy && PooledObjects[i].Tag == tag)
            {
                return PooledObjects[i].gameObject;
            }
        }
        foreach (ObjectToPoolPreset item in ObjectsToPool)
        {
            if (item.ObjectToPool.Tag == tag)
            {
                if (item.shouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.ObjectToPool.gameObject);
                    obj.SetActive(false);
                    PooledObjects.Add(obj.GetComponent<PoolableObject>());
                    obj.transform.SetParent(transform);

                    return obj;
                }
            }
        }
        return null;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
