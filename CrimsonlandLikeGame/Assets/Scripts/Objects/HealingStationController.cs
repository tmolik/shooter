﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingStationController : MonoBehaviour
{

    public void Update()
    {
        float distance = Vector3.Distance(PlayerController.Get.transform.position, transform.position);

        if (distance<15)
            PlayerController.Get.Parameters.Health = Mathf.Min(PlayerController.Get.Parameters.Health + 5 * Time.deltaTime, PlayerController.Get.Parameters.GetMaxHealth());
    }
}
