﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for every object on scene that can be hit by bullet
/// </summary>
public class HittableObject : MonoBehaviour
{
    /// <summary>Object's heatlh</summary>
    public float Health;

    public bool IsAlive = true;

    /// <summary>Helper value to invoke OnZeroHealth function only once</summary>
    internal bool InvokedZeroHealthFunction = false;

    public bool IsOnFire;

    internal float OnFireTimer;

    internal Transform FireTransform;

    internal Transform IceTransform;

    internal FireOffset FireOffset;

    public virtual void Start()
    {
        GameController.Get.Hittables.Add(this);

        FireOffset = GetComponent<FireOffset>();
    }

    public virtual void Update()
    {
        if (IsOnFire)
        {
            if (IsAlive)
                OnHit(2 * Time.deltaTime);

            if (FireTransform != null)
                FireTransform.position = transform.position + (FireOffset != null ? FireOffset.Offset : Vector3.zero);

            OnFireTimer += Time.deltaTime;

            if (OnFireTimer > 5)
            {
                IsOnFire = false;
                //FireTransform.gameObject.SetActive(false);
                ParticleSystem ps = FireTransform.transform.GetComponent<ParticleSystem>();
                var emission = ps.emission;
                var rate = emission.rateOverTime;
                rate.constantMax = 0f;
                emission.rateOverTime = rate;
                StartCoroutine(DisableFireTransform());
            }
        }

    }

    internal IEnumerator DisableFireTransform()
    {
        yield return new WaitForSeconds(2);
        if (!IsOnFire)
            FireTransform.gameObject.SetActive(false);
    }

    public virtual bool SetOnFire()
    {
        if (!IsAlive)
            return false;

        OnFireTimer = 0;
        if (!IsOnFire)
        {
            IsOnFire = true;
            GameObject fire = ObjectPooler.Get.GetPooledObject("Fire");

            
            fire.transform.localScale = (FireOffset != null && FireOffset.FireScale!=-1) ? Vector3.one* FireOffset.FireScale : transform.localScale;
            fire.transform.position = transform.position + (FireOffset != null ? FireOffset.Offset : Vector3.zero);
            fire.GetComponent<FireController>().FireBearer = this;
            fire.gameObject.SetActive(true);
            FireTransform = fire.transform;

            ParticleSystem ps = fire.transform.GetComponent<ParticleSystem>();
            var emission = ps.emission;
            var rate = emission.rateOverTime;
            rate.constantMax = 25f;
            emission.rateOverTime = rate;

            return true;
        }

        return false;
    }

    /// <summary>
    /// Function invoked when object is hit by bullet
    /// Returns hittable alive, true if is alive, false if is not
    /// </summary>
    /// <param name="damage"></param>
    public virtual bool OnHit(float damage)
    {
        Health -= damage;
        if (Health <= 0 && !InvokedZeroHealthFunction)
        {
            InvokedZeroHealthFunction = true;
            IsAlive = false;
            OnZeroHealth();
        }
        return IsAlive;
    }

    /// <summary>
    /// Function invoked when object's health is zero, by default is destroys itself
    /// </summary>
    public virtual void OnZeroHealth()
    {
        Destroy(gameObject);
    }

    public virtual void OnDestroy()
    {
        GameController.Get.Hittables.Remove(this);
    }
}
