﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampfireController : MonoBehaviour
{

    public void OnTriggerEnter(Collider other)
    {
        HittableObject hittable = other.GetComponent<HittableObject>();

        if (hittable != null)
        {
            hittable.SetOnFire();
        }

    }
}
