﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineController : HittableObject
{

    public void OnTriggerEnter(Collider other)
    {
        HittableObject hittable = other.GetComponent<HittableObject>();
        if (hittable != null)
        {
            OnZeroHealth();
        }
    }

    public override void OnZeroHealth()
    {
        GameObject explode = ObjectPooler.Get.GetPooledObject("Explode");
        explode.transform.position = transform.position;
        explode.gameObject.SetActive(true);
        explode.GetComponent<Explosion>().Explode();

        gameObject.SetActive(false);
    }

    public void OnDisable()
    {
        Health = 10;
        InvokedZeroHealthFunction = false;
    }
}
