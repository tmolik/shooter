﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for environment elements that should block bullets but do nothing more about it, they are hittable but not destroyable
/// </summary>
public class Hittable_Enviro : HittableObject
{

    public override bool SetOnFire()
    {
        return false;
    }

    public override void Start()
    {
    }

    public override bool OnHit(float damage)
    {
        return true;
    }

    public override void OnDestroy()
    {
    }
}
