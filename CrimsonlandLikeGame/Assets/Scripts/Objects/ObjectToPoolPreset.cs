﻿using UnityEngine;

[System.Serializable]
public class ObjectToPoolPreset
{
    public int AmountToPool;
    public PoolableObject ObjectToPool;
    public bool shouldExpand;
}
