﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{

    public string LabelToShow;

    public Vector3 LabelOffset;

    public bool PlayerInteracting = false;

    private bool showingUi;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public void Update()
    {
        float playerDistance = Vector3.Distance(PlayerController.Get.transform.position, transform.position);

        if(playerDistance<5 && !showingUi && !PlayerInteracting)
        {
            ShowInteractUi();
        }

        if(playerDistance > 5.5f && showingUi)
        {
            HideInteractUi();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (PlayerInteracting)
            {
                StopInteracting();
            }
            else
            {
                if (Vector3.Distance(PlayerController.Get.transform.position, transform.position) < 5)
                {
                    Interact();
                    HideInteractUi();
                }
            }
        }
    }

    public void ShowInteractUi()
    {
        Debug.Log("Can interact now");
        showingUi = true;
        UiInteractable.Get.Show(LabelToShow, transform, LabelOffset);
    }

    public void HideInteractUi()
    {
        Debug.Log("Cant interact now");
        showingUi = false;
        UiInteractable.Get.Hide();
    }

    public void Interact()
    {
        //Checking conditions here
        OnStartedInteraction();
        PlayerInteracting = true;
        PlayerController.Get.CurrentlyUsedInteractable = this;
    }

    public void StopInteracting()
    {
        OnFinishedInteraction();
        PlayerInteracting = false;
        PlayerController.Get.CurrentlyUsedInteractable = null;

    }

    public virtual void OnStartedInteraction()
    {
        Debug.Log("Started interaction");
    }

    public virtual void OnFinishedInteraction()
    {
        Debug.Log("Finished interaction");
    }
}
