﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for explosive barrel object
/// </summary>
public class ExplosiveBarrel : HittableObject
{
    public AudioSource BarrelAudioSource;

    /// <summary>
    /// Function invoked when object's health is zero
    /// </summary>
    public override void OnZeroHealth()
    {
        //GameObject explode = Instantiate(PrefabsHelper.Get.BarrelExplode);
        GameObject explode = ObjectPooler.Get.GetPooledObject("BarrelExplosion");
        explode.transform.position = transform.position;
        explode.gameObject.SetActive(true);
        explode.GetComponent<Explosion>().Explode();
        AddRigidbodyAndForceToAllChildren();
        gameObject.AddComponent<DestroyAfterTime>().TimeToDestroy = 10f;

        GetComponent<Collider>().enabled = false;

        BarrelAudioSource.PlayOneShot(AudioManager.Get.BarrelExplosionClips.GetRandomAudioClip());
        GameStatsController.DestroyedExplosiveBarrel();
    }

    /// <summary>
    /// Adds rigidbodies to its children and adds some force to imitate explosion force
    /// </summary>
    public void AddRigidbodyAndForceToAllChildren()
    {
        for (int i = 0; i < transform.childCount-1; i++)
        {
            Rigidbody rigidbody = transform.GetChild(i).gameObject.AddComponent<Rigidbody>();
            rigidbody.AddForce(new Vector3(Random.Range(-1f,1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f))* 6, ForceMode.Impulse);
        }
    }

}
