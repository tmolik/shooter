﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Get { get; private set; }

    public AudioManager()
    {
        Get = this;
    }

    public AudioSource ManagerAudioSource;
    public AudioSource MusicAudioSource;

    public AudioClipsCollection BarrelExplosionClips;
    public AudioClipsCollection SpiderClips;
    public AudioClipsCollection SpiderChatClips;
    public AudioClip EnemyGetHitClip;


    public AudioClip SpiderNestDestroyClip;

    public AudioClip AchievementUnlockedClip;

    public AudioClip CollectedSlowTime;
    public AudioClip CollectedWeapon;
    public AudioClip CollectedPill;

    public AudioClip AfterExplosionClip;
    // Use this for initialization
    void Start()
    {
        GameObject.DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (SettingsManager.CurrentSettings.MusicOn)
        {
            if (UIMainMenuController.Get.Showing)
                MusicAudioSource.volume = Mathf.Lerp(MusicAudioSource.volume, 0, 8 * Time.unscaledDeltaTime);
            else
                MusicAudioSource.volume = Mathf.Lerp(MusicAudioSource.volume, 0.1f, 8 * Time.unscaledDeltaTime);
        }
        else
        {
            MusicAudioSource.volume = 0;
        }
    }

    public void PlayClipAtPosition(Vector3 position, AudioClip clip, float spatialBlend=1)
    {
        GameObject empty = new GameObject();
        empty.transform.position = position;
        AudioSource audioSource = empty.AddComponent<AudioSource>();
        audioSource.spatialBlend = spatialBlend;
        audioSource.PlayOneShot(clip);
        empty.AddComponent<DestroyAfterTime>().TimeToDestroy = 5;

    }

    public void PlayAchievementSound()
    {
        ManagerAudioSource.PlayOneShot(AchievementUnlockedClip);
    }

    public void PlayCollectedSlowTime(Vector3 position)
    {
        PlayClipAtPosition(position, CollectedSlowTime, 0.5f);
    }

    public void PlayCollectedWeapon(Vector3 position)
    {
        PlayClipAtPosition(position, CollectedWeapon);
    }

    public void PlayCollectedPill(Vector3 position)
    {
        PlayClipAtPosition(position, CollectedPill);
    }

    public void PlayAfterExplosionClip()
    {
        GameObject empty = new GameObject();
        AudioSource audioSource = empty.AddComponent<AudioSource>();
        audioSource.spatialBlend = 0;
        audioSource.volume = 0.4f;
        audioSource.pitch = 0.9f;
        audioSource.PlayOneShot(AfterExplosionClip);
        empty.AddComponent<DestroyAfterTime>().TimeToDestroy = 5;
    }
}
