﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudioController : MonoBehaviour
{
    public static PlayerAudioController Get { get; private set; }

    public PlayerAudioController()
    {
        Get = this;
    }

    public AudioSource PlayerAudioSource;
    public AudioSource HeartAudioSource;

    public AudioClipsCollection PistolClips;
    public AudioClipsCollection UziClips;
    public AudioClipsCollection ShotgunClips;
    public AudioClipsCollection SniperClips;
    public AudioClipsCollection RocketLauncherClips;

    public AudioClip ReloadClip;

    public AudioClipsCollection GetHitClips;
    public AudioClip DeathClip;

    private float helperTimer = 0;

    public void PlayShotSound(EWeaponType weaponType)
    {
        AudioClip clip = null;
        switch (weaponType)
        {
            case EWeaponType.HeavyMachineGun:
            case EWeaponType.Pistol:
                clip = PistolClips.GetRandomAudioClip();
                break;
            case EWeaponType.Uzi:
                clip = UziClips.GetRandomAudioClip();
                break;
            case EWeaponType.Shotgun:
                clip = ShotgunClips.GetRandomAudioClip();
                break;
            case EWeaponType.Sniper:
                clip = SniperClips.GetRandomAudioClip();
                break;
            case EWeaponType.RocketLauncher:
                clip = RocketLauncherClips.GetRandomAudioClip();
                break;
            default:
                break;
        }

        if (clip != null)
        {
            PlayerAudioSource.pitch = 1 + Random.Range(-0.05f, 0.05f);
            PlayerAudioSource.PlayOneShot(clip);
        }
    }

    public void Update()
    {
        float playerHealthPercent = PlayerController.Get.Parameters.Health / PlayerController.Get.Parameters.GetMaxHealth();
        helperTimer += Time.deltaTime;
        if (helperTimer > 0.65f)
        {
            HeartAudioSource.Play();
            helperTimer = 0;
        }
        if (playerHealthPercent > 0)
        {
            HeartAudioSource.volume = Mathf.Lerp(0.25f, 0f, playerHealthPercent / 0.3f);
        }
        else
            HeartAudioSource.volume = 0;

    }

    public void PlayReload()
    {
        StartCoroutine(PlayReloadAfterTime(0.4f));
    }

    private IEnumerator PlayReloadAfterTime(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        PlayerAudioSource.PlayOneShot(ReloadClip);

    }

    public void PlayGetHit()
    {
        PlayerAudioSource.PlayOneShot(GetHitClips.GetRandomAudioClip());
    }

    public void PlayDeath()
    {
        PlayerAudioSource.PlayOneShot(DeathClip);
    }

}
