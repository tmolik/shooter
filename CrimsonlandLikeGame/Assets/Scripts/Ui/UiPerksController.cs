﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiPerksController : MonoBehaviour
{
    public static UiPerksController Get { get; private set; }

    public UiPerksController()
    {
        Get = this;
    }

    public UiPerkSlot FirstPerkSlot;
    public UiPerkSlot SecondPerkSlot;
    public UiPerkSlot ThirdPerkSlot;

    public RectTransform MainPanel;

    private bool animatingIn;
    private bool animatingOut;
    private bool showing;

    private float helperTimer = 0;

    // Update is called once per frame
    void Update()
    {
        if (animatingIn)
        {
            helperTimer += Time.unscaledDeltaTime;
            float easingValue = EasingFunction.EaseOutBack(0, 1, helperTimer / 0.6f);
            MainPanel.anchoredPosition = Vector2.LerpUnclamped(new Vector2(0, MainPanel.sizeDelta.y / 2), new Vector2(0,-540), easingValue);

            //MainPanel.anchoredPosition = Vector2.LerpUnclamped(new Vector2(0, 800), Vector2.zero,easingValue);

            if (helperTimer >= 0.6f)
            {
                animatingIn = false;
                showing = true;
            }
        }

        if (animatingOut)
        {
            helperTimer += Time.unscaledDeltaTime;
            float easingValue = EasingFunction.EaseOutBack(0, 1, helperTimer / 0.6f);

            //MainPanel.anchoredPosition = Vector2.LerpUnclamped( Vector2.zero, new Vector2(0, 800), easingValue);
            MainPanel.anchoredPosition = Vector2.LerpUnclamped( new Vector2(0, -540),new Vector2(0, MainPanel.sizeDelta.y / 2), easingValue);

            if (helperTimer >= 0.6f)
            {
                animatingOut = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            ShowPerks();
        }
    }

    public void ShowPerks()
    {
        PreparePerksSlots();
        helperTimer = 0;
        animatingIn = true;
        CrosshairController.Get.SwitchToCursor();
        MainController.Get.PauseGame();
    }

    public void Hide()
    {
        animatingOut = true;
        animatingIn = false;
        helperTimer = 0;
        CrosshairController.Get.SwitchToCrosshair();
        MainController.Get.UnpauseGame();
        showing = false;

    }

    private void PreparePerksSlots()
    {
        List<Perk> perks = PerksController.GetThreeRandomPerks();
        FirstPerkSlot.SetData(perks[0]);
        SecondPerkSlot.SetData(perks[1]);
        ThirdPerkSlot.SetData(perks[2]);
    }

    public bool CanShoot()
    {
        if (animatingIn || animatingOut || showing)
            return false;
        else
            return true;
    }
    
}
