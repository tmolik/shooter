﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiInteractable : MonoBehaviour
{

    public static UiInteractable Get { get; private set; }

    public UiInteractable()
    {
        Get = this;
    }

    internal Text LabelText;

    internal RectTransform MainRect;

    public Transform TargetInteractable;

    public Vector3 Offset;

    internal CanvasGroup InteractablesCanvasGroup;

    private bool showing;

    // Use this for initialization
    void Start()
    {
        LabelText = GetComponent<Text>();
        MainRect = GetComponent<RectTransform>();
        InteractablesCanvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (TargetInteractable != null)
            MainRect.position = CameraController.Get.CameraComponent.WorldToScreenPoint(TargetInteractable.transform.position + Offset);

        if (showing)
            InteractablesCanvasGroup.alpha = 1;

        if (GameEventsManager.Get.TimelinePlaying)
            InteractablesCanvasGroup.alpha = 0;
    }

    public void Show(string label, Transform targetTransform, Vector3 offset)
    {
        TargetInteractable = targetTransform;
        LabelText.text = label;
        Offset = offset;
        InteractablesCanvasGroup.alpha = 1;
        showing = true;
    }

    public void Hide()
    {
        InteractablesCanvasGroup.alpha = 0;
        showing = false;
    }
}
