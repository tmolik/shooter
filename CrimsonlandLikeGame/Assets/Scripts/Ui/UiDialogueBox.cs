﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiDialogueBox : MonoBehaviour {

    internal Text ContentText;

    internal RectTransform MainRect;

    public Transform TargetInteractable;

    public Vector3 Offset;

    private float showTime;
    private float helperTimer;


    // Update is called once per frame
    void Update()
    {
        if (TargetInteractable != null)
            MainRect.position = CameraController.Get.CameraComponent.WorldToScreenPoint(TargetInteractable.transform.position + Offset);

        helperTimer += Time.deltaTime;
        if (helperTimer >= showTime)
        {
            Destroy(gameObject);
        }
    }


    public void Show(string label, Transform targetTransform, Vector3 offset, float time)
    {
        MainRect = GetComponent<RectTransform>();
        ContentText = GetComponent<Text>();
        TargetInteractable = targetTransform;
        ContentText.text = label;
        Offset = offset;
        showTime = time;
        helperTimer = 0;
    }
}
