﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class for ui representing of just achieved achievements in left down corner of game
/// </summary>
public class UiNewAchievementController : MonoBehaviour
{
    public static UiNewAchievementController Get { get; private set; }

    public UiNewAchievementController()
    {
        Get = this;
    }

    /// <summary>
    /// Queued achievements
    /// </summary>
    public Queue AchievementsToShow = new Queue();

    /// <summary> Main achievement rect transform </summary>
    public RectTransform MainRect;

    /// <summary> Canvas group of whole achievement panel </summary>
    public CanvasGroup MainCanvasGroup;

    /// <summary> Canvas group on title and description </summary>
    public CanvasGroup AchievementCanvasGroup;

    public Text TitleText;

    public Text DescriptionText;
    
    /// <summary> Is currently animating any achievement </summary>
    public bool AnimatingAchievement = false;


    private float helperTimer;

    // Update is called once per frame
    void Update()
    {
        if (!AnimatingAchievement)
        {
            if (AchievementsToShow.Count > 0)
            {
                Achievement achievement = (Achievement)AchievementsToShow.Dequeue();
                TitleText.text = achievement.AchievementName;
                DescriptionText.text = achievement.Description;
                AnimatingAchievement = true;
                helperTimer = 0;

                AchievementCanvasGroup.alpha = 0;
                AudioManager.Get.PlayAchievementSound();
            }
            else
            {
                MainCanvasGroup.alpha = Mathf.Lerp(MainCanvasGroup.alpha, 0, Time.deltaTime * 4);
                MainRect.anchoredPosition = Vector2.Lerp(MainRect.anchoredPosition, new Vector2(-400,0), Time.deltaTime * 4);
            }
        }
        else
        {
            MainCanvasGroup.alpha = Mathf.Lerp(MainCanvasGroup.alpha, 1, Time.deltaTime * 6);
            MainRect.anchoredPosition = Vector2.Lerp(MainRect.anchoredPosition, Vector2.zero, Time.deltaTime * 6);


            helperTimer += Time.deltaTime;

            if (helperTimer < 0.8f)
            {
                float easingValue = EasingFunction.EaseInSine(0, 1, helperTimer / 0.8f);
                AchievementCanvasGroup.alpha = Mathf.Lerp(0, 1, easingValue);

            }

            if (helperTimer > 2.3f)
            {
                float easingValue = EasingFunction.EaseInSine(0, 1, (helperTimer - 2.3f) / 0.8f);
                AchievementCanvasGroup.alpha = Mathf.Lerp(1,0, easingValue);

            }

            if (helperTimer >2.5f)
            {
                AnimatingAchievement = false;
            }
        }
    }

    public void AddAchievementToQueue(Achievement achievement)
    {
        AchievementsToShow.Enqueue(achievement);
    }
}
