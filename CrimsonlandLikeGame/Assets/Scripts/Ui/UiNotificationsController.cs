﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class for simple text notifications queued to show one after another(it there are more than one)
/// </summary>
public class UiNotificationsController : MonoBehaviour
{
    public static UiNotificationsController Get { get; private set; }

    public UiNotificationsController()
    {
        Get = this;
    }

    public Text NotificationText;

    public RectTransform NotificationRect;

    public Queue NotificationsToShow = new Queue();

    private bool animating = false;
    private float helperTimer = 0;


    // Update is called once per frame
    void Update()
    {
        if (animating)
        {
            helperTimer += Time.deltaTime;
            if (helperTimer <= 0.5f)
            {
                float easingValue = EasingFunction.EaseInOutCirc(0, 1, helperTimer / 0.5f);
                NotificationRect.localScale = Vector3.LerpUnclamped(Vector3.zero, Vector3.one, easingValue);
            }
            if (helperTimer > 1.4f)
            {
                float easingValue = EasingFunction.EaseInQuart(0, 1, (helperTimer-1.4f) / 0.7f);
                NotificationRect.localScale = Vector3.LerpUnclamped(Vector3.one, Vector3.zero, easingValue);
            }
            if (helperTimer > 2.1f)
            {
                NotificationRect.localScale = Vector3.zero;
                animating = false;
            }
        }
        else
        {

            if (NotificationsToShow != null)
            {
                if (NotificationsToShow.Count > 0)
                {
                    string notification = (string)NotificationsToShow.Dequeue();
                    NotificationText.text = notification;
                    animating = true;
                    helperTimer = 0;
                }
            }
        }
    }

    public void ShowNotification(string text)
    {
        NotificationsToShow.Enqueue(text);
    }
}
