﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiSettingsPanel : MonoBehaviour
{

    public static UiSettingsPanel Get { get; private set; }

    public UiSettingsPanel()
    {
        Get = this;
    }

    public Slider MasterVolumeSlider;

    public Toggle MusicOnToggle;

    public void Start()
    {
        SetComponentsValuesFromSettings();
    }

    public void SetComponentsValuesFromSettings()
    {
        MasterVolumeSlider.value = SettingsManager.CurrentSettings.MasterVolume;
    }

    public void OnVolumeSliderChange()
    {
        SettingsManager.ChangedMasterVolumeSlider(MasterVolumeSlider.value);
    }

    public void OnMusicToggleChange()
    {
        SettingsManager.CurrentSettings.MusicOn = MusicOnToggle.isOn;
    }

    public void OnClickGoBack()
    {
        gameObject.SetActive(false);
    }
}
