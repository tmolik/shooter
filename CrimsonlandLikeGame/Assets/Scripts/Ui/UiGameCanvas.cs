﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiGameCanvas : MonoBehaviour
{
    public static UiGameCanvas Get { get; private set; }

    public UiGameCanvas()
    {
        Get = this;
    }


    public RectTransform GameCanvasRect;

    // Use this for initialization
    void Start()
    {
        GameCanvasRect = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
