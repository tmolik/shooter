﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiIndicatorsController : MonoBehaviour
{
    public static UiIndicatorsController Get { get; private set; }

    public UiIndicatorsController()
    {
        Get = this;
    }


    public bool ShowingIndicators = false;
    private float helperTimer = 0;

    public Color EnemyIndicatorColor;

    public Color NestIndicatorColor;

    public void ShowIndicators()
    {
        if (ShowingIndicators)
            return;

        foreach(EnemySpawner spawner in LevelInfo.Get.Spawners)
        {
            UiTargetIndicator indicator = ObjectPooler.Get.GetPooledObject("TargetIndicator").GetComponent<UiTargetIndicator>();
            indicator.transform.SetParent(transform);
            indicator.SetData(spawner.transform, false);
            indicator.gameObject.SetActive(true);
        }

        foreach (EnemyController enemy in LevelInfo.Get.Enemies)
        {
            UiTargetIndicator indicator = ObjectPooler.Get.GetPooledObject("TargetIndicator").GetComponent<UiTargetIndicator>();
            indicator.transform.SetParent(transform);
            indicator.SetData(enemy.transform, true);
            indicator.gameObject.SetActive(true);
        }

        helperTimer = 0;
        ShowingIndicators = true;
    }


    // Update is called once per frame
    void Update()
    {
        if (ShowingIndicators)
        {
            helperTimer += Time.deltaTime;

            if (helperTimer >= 10)
                ShowingIndicators = false;
        }
    }
}
