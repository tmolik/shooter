﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Ui for achievements list window
/// </summary>
public class UiAchievementsList : MonoBehaviour
{
    public static UiAchievementsList Get { get; private set; }

    public UiAchievementsList()
    {
        Get = this;
    }

    /// <summary> Is this window showed right now </summary>
    public static bool Showing;

    /// <summary> PRefab of single achievement </summary>
    public GameObject UiAchievementPrefab;

    /// <summary> Achievements content rect transform </summary>
    public RectTransform ContentRect;

    /// <summary> Main RectTransform</summary>
    public RectTransform MainRect;

    /// <summary> Instantiated achievements instances </summary>
    public List<UiAchievement> UiAchievements = new List<UiAchievement>();

    public Color AchievedColor;
    public Color NotAchievedColor;


    public void Start()
    {
        CreateAchievements(AchievementsController.CurrentAchievements.Achievements);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            Show();
        }
    }

    /// <summary>
    /// Create instances for achievements in given list, set completed achievemtns first in order they were achieved
    /// </summary>
    /// <param name="achievements"></param>
    public void CreateAchievements(List<Achievement> achievements)
    {
        List<Achievement> achievementsNotAchieved = achievements.Where(a => !a.IsAchieved).ToList();

        List<Achievement> achievementsAchieved = achievements.Where(a => a.IsAchieved).OrderBy(a => a.AchievedAt).ToList();

        foreach(Achievement achievement in achievementsNotAchieved)
        {
            UiAchievement uiAchievement = Instantiate(UiAchievementPrefab).GetComponent<UiAchievement>();
            uiAchievement.transform.SetParent(ContentRect);
            uiAchievement.transform.SetAsFirstSibling();
            uiAchievement.SetData(achievement);
            uiAchievement.transform.localScale = Vector3.one;
        }

        foreach (Achievement achievement in achievementsAchieved)
        {
            UiAchievement uiAchievement = Instantiate(UiAchievementPrefab).GetComponent<UiAchievement>();
            uiAchievement.transform.SetParent(ContentRect);
            uiAchievement.transform.SetAsFirstSibling();
            uiAchievement.SetData(achievement);
            uiAchievement.transform.localScale = Vector3.one;

        }
    }

    public void AchievementUnlocked(Achievement achievement)
    {
        foreach(UiAchievement uiAchievement in UiAchievements)
        {
            if(uiAchievement.TitleText.text == achievement.AchievementName)
            {
                uiAchievement.SetAchieved();
                uiAchievement.transform.SetAsFirstSibling();
                return;
            }
        }
    }

    public void ClickedQuit()
    {
        MainRect.gameObject.SetActive(false);
        CrosshairController.Get.SwitchToCrosshair();
        Showing = false;
    }

    public void Show()
    {
        MainRect.gameObject.SetActive(true);
        CrosshairController.Get.SwitchToCursor();
        Showing = true;
    }


}
