﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiObjectivesController : MonoBehaviour
{
    public static UiObjectivesController Get { get; private set; }

    public UiObjectivesController()
    {
        Get = this;
    }

    public CanvasGroup ObjectivesCanvasGroup;

    public Text ObjectiveText;

    private float helperTimer;
    private bool animating;


    // Update is called once per frame
    void Update()
    {
        if (animating)
        {
            helperTimer += Time.deltaTime;

            if(helperTimer < 0.5f)
            {
                ObjectivesCanvasGroup.alpha = Mathf.Lerp(0, 1, helperTimer / 0.5f);
            }

            if (helperTimer > 1.5f)
            {
                ObjectivesCanvasGroup.alpha = Mathf.Lerp(1, 0, (helperTimer - 1.5f) / 0.5f);
            }

            if(helperTimer >2f)
            {
                animating = false;
                ObjectivesCanvasGroup.alpha = 0;
            }
        }
    }

    public void ShowObjective(Objective objective)
    {
        ObjectiveText.text = objective.Description;
        helperTimer = 0;
        animating = true;
    }
}
