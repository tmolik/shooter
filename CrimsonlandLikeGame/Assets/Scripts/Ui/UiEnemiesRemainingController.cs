﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiEnemiesRemainingController : MonoBehaviour
{


    public RectTransform EnemiesRemainingRect;

    public Image EnemiesRemainingFill;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!MainController.LevelLoaded)
            return;

        if (LevelInfo.Get == null)
            return;

        if (MainController.Get.CurrentLevelPreset == null)
            return;

        if (LevelInfo.Get.GameMode != EGameMode.Normal)
        {
            if (EnemiesRemainingRect.gameObject.activeInHierarchy)
                EnemiesRemainingRect.gameObject.SetActive(false);
        }
        else
        {
            if (!EnemiesRemainingRect.gameObject.activeInHierarchy)
                EnemiesRemainingRect.gameObject.SetActive(true);

            float enemiesCount = LevelInfo.Get.Enemies.Count;
            float startingEnemiesCount = MainController.Get.CurrentLevelPreset.GetAllEnemiesCount();

            float spawnerCount = LevelInfo.Get.Spawners.Count;
            float startingSpawnersCount = MainController.Get.CurrentLevelPreset.GetAllNestsCount();

            EnemiesRemainingFill.fillAmount = Mathf.Lerp(EnemiesRemainingFill.fillAmount, (enemiesCount + spawnerCount) / (startingEnemiesCount + startingSpawnersCount), Time.deltaTime*4);
        }
    }
}
