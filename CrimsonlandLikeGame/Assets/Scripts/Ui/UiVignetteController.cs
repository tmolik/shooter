﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class to control vignette ui that is shown when player is hit.
/// </summary>
public class UiVignetteController : MonoBehaviour
{
    public static UiVignetteController Get { get; set; }

    public UiVignetteController()
    {
        Get = this;
    }

    internal Image VignetteImage;

    private float helperTimer = 0;
    private bool flashing = false;

    // Use this for initialization
    void Start()
    {
        VignetteImage = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (flashing)
        {
            helperTimer += Time.deltaTime;
            float alphaValue = Mathf.Lerp(0,0.5f, helperTimer / 0.1f);
            VignetteImage.color = new Color(VignetteImage.color.r, VignetteImage.color.g, VignetteImage.color.b, alphaValue);
            if (helperTimer >= 0.1f)
                flashing = false;
        }
        else
        {
            float alphaValue = Mathf.Lerp(VignetteImage.color.a,0, Time.deltaTime*6);
            VignetteImage.color = new Color(VignetteImage.color.r, VignetteImage.color.g, VignetteImage.color.b, alphaValue);
        }
    }

    public void GetHit()
    {
        if (flashing)
            return;
        helperTimer = 0;
        flashing = true;
    }
}
