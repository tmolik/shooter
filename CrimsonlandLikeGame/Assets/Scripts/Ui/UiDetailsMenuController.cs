﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiDetailsMenuController : MonoBehaviour
{

    public RectTransform CreditsRect;
    public RectTransform AchievementsRect;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClickGoBack()
    {
        gameObject.SetActive(false);
    }

    public void OnClickCredits()
    {
        CreditsRect.gameObject.SetActive(true);
    }

    public void OnClickGoBackFromCredits()
    {
        CreditsRect.gameObject.SetActive(false);
    }


    public void OnClickAchievements()
    {
       AchievementsRect.gameObject.SetActive(true);
    }

    public void OnClickGoBackFromAchievements()
    {
        AchievementsRect.gameObject.SetActive(false);
    }

}
