﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiLoadingScreenController : MonoBehaviour
{
    public static UiLoadingScreenController Get { get; set; }

    public UiLoadingScreenController()
    {
        Get = this;
    }


    internal CanvasGroup CanvasGroup;

    private bool showing;

    // Use this for initialization
    void Start()
    {
        CanvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (showing)
        {
            CanvasGroup.alpha = Mathf.Lerp(CanvasGroup.alpha, 1, 8 * Time.deltaTime);
        }
        else
        {
            CanvasGroup.alpha = Mathf.Lerp(CanvasGroup.alpha, 0, 8 * Time.deltaTime);

        }
    }

    public void FadeIn()
    {
        showing = true;
        CanvasGroup.blocksRaycasts = true;
    }

    public void FadeOut()
    {
        showing = false;
        CanvasGroup.blocksRaycasts = false;
    }
}
