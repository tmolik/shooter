﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiPerkSlot : MonoBehaviour
{
    public Text TitleText;

    public Text DescriptionText;

    public Text CurrentValueText;

    public Text NextLevelValueText;

    public EPerkType RepresentedPerkType;

    public void SetData(Perk perk)
    {
        TitleText.text = perk.Title;
        DescriptionText.text = perk.Description;
        CurrentValueText.text = "Current value: " + perk.CurrentValue.ToString() + "%";
        NextLevelValueText.text = "Next value: " + (perk.CurrentValue + perk.ValueIncreasedPerLevel).ToString() + "%";
        RepresentedPerkType = perk.PerkType;
    }

    public void OnClickPerk()
    {
        PerksController.LevelUpPerk(RepresentedPerkType);
        UiPerksController.Get.Hide();
    }
}
