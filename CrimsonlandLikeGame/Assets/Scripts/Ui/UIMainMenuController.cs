﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Class to control main menu
/// </summary>
public class UIMainMenuController : MonoBehaviour
{
    public static UIMainMenuController Get { get; set; }

    public UIMainMenuController()
    {
        Get = this;
    }

    public CanvasGroup MainMenuCanvasGroup;
    public RectTransform MainRect;

    public RectTransform LevelsRect;
    public RectTransform DetailsRect;
    public RectTransform SettingsRect;

    public RectTransform ResumeGameButtonRect;

    public bool Showing = true;

    private bool fadingIn = false;
    private bool fadingOut = false;
    private float helperTimer;

    public AudioSource MenuMusicAudioSource;


    // Update is called once per frame
    void Update()
    {
        if (fadingIn)
        {
            helperTimer += Time.unscaledDeltaTime;

            MainMenuCanvasGroup.alpha = Mathf.Lerp(0, 1, helperTimer / 0.5f);

            MenuMusicAudioSource.volume = Mathf.Lerp(0, 0.2f, helperTimer / 0.5f);

            if (helperTimer >= 0.5f)
            {
                fadingIn = false;
                Showing = true;
            }
        }

        if (fadingOut)
        {
            helperTimer += Time.unscaledDeltaTime;

            MainMenuCanvasGroup.alpha = Mathf.Lerp(1, 0, helperTimer / 0.5f);

            MenuMusicAudioSource.volume = Mathf.Lerp(0.2f, 0, helperTimer / 0.5f);


            if (helperTimer >= 0.5f)
            {
                fadingOut = false;
                MainRect.gameObject.SetActive(false);
                MenuMusicAudioSource.volume = 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (MainController.LoadingLevel)
                return;

            if (!MainController.LevelLoaded)
                return;

            if (Showing || fadingIn)
                FadeOut();
            else
                FadeIn();
        }

        if (!SettingsManager.CurrentSettings.MusicOn)
            MenuMusicAudioSource.volume = 0;
        else
        {
            if (Showing && !fadingIn)
                MenuMusicAudioSource.volume = 0.2f;
        }
    }

    /// <summary>
    /// Fade in menu, pause game and switch cursor
    /// </summary>
    public void FadeIn()
    {
        MainRect.gameObject.SetActive(true);

        helperTimer = 0;
        fadingIn = true;
        fadingOut = false;
        ResumeGameButtonRect.gameObject.SetActive(MainController.LevelLoaded);
        MainController.Get.PauseGame();
        CrosshairController.Get.SwitchToCursor();
    }


    /// <summary>
    /// Fade out menu, unpause game and switch cursor
    /// </summary>
    public void FadeOut()
    {
        helperTimer = 0;
        fadingOut = true;
        Showing = false;
        MainController.Get.UnpauseGame();
        CrosshairController.Get.SwitchToCrosshair();
    }

    public void OnClickNewGame()
    {
        LevelsRect.gameObject.SetActive(true);
    }

    public void OnClickDetails()
    {
        DetailsRect.gameObject.SetActive(true);
    }

    public void OnClickSettings()
    {
        SettingsRect.gameObject.SetActive(true);
    }

    public void OnClickResumeGame()
    {
        if (!fadingOut)
            FadeOut();
    }

    public void OnClickQuit()
    {
        Application.Quit();
    }

    public void OnClickFilip()
    {
        MainController.Get.TestGame();
    }

    public void OnClickSurvivalMode()
    {
        MainController.Get.SurvivalGame();
    }

    public bool CanShoot()
    {
        if (fadingIn || fadingOut || Showing)
            return false;
        else
            return true;
    }
}
