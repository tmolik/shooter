﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Ui for displaying just completed achievement. It is not used now and represented by a new one.
/// </summary>
public class UiAchievementsController : MonoBehaviour
{
    public static UiAchievementsController Get { get; private set; }

    public UiAchievementsController()
    {
        Get = this;
    }

    public Queue AchievementsToShow = new Queue();

    public RectTransform AchievementRect;

    public CanvasGroup AchievementCanvasGroup;

    public Text TitleText;

    public Text DescriptionText;

    public bool AnimatingAchievement = false;


    private float helperTimer;

    // Update is called once per frame
    void Update()
    {


        if (!AnimatingAchievement)
        {
            if (AchievementsToShow.Count > 0)
            {
                Achievement achievement = (Achievement)AchievementsToShow.Dequeue();
                TitleText.text = achievement.AchievementName;
                DescriptionText.text = achievement.Description;
                AnimatingAchievement = true;
                helperTimer = 0;
                AchievementRect.anchoredPosition = new Vector2(-AchievementRect.sizeDelta.x, 0);
                AudioManager.Get.PlayAchievementSound();
            }
            else
            {
                AchievementCanvasGroup.alpha = Mathf.Lerp(AchievementCanvasGroup.alpha, 0, Time.deltaTime * 6);
            }
        }
        else
        {
            AchievementCanvasGroup.alpha = Mathf.Lerp(AchievementCanvasGroup.alpha, 1, Time.deltaTime * 6);


            helperTimer += Time.deltaTime;

            if (helperTimer < 0.8f)
            {
                float easingValue = EasingFunction.EaseOutBack(0, 1, helperTimer / 0.8f);
                AchievementRect.anchoredPosition = Vector2.LerpUnclamped(new Vector2(-AchievementRect.sizeDelta.x, 0), Vector2.zero, easingValue);
            }
            
            if(helperTimer > 2.3f)
            {
                float easingValue = EasingFunction.EaseInBack(0, 1, (helperTimer-2.3f) / 0.8f);
                AchievementRect.anchoredPosition = Vector2.LerpUnclamped( Vector2.zero, new Vector2(AchievementRect.sizeDelta.x, 0), easingValue);
            }


            if (helperTimer > 3.1f)
            {
                AnimatingAchievement = false;
            }
        }


        if (Input.GetKeyDown(KeyCode.O))
        {
            //AddAchievementToQueue(AchievementsController.CurrentAchievements.Achievements[Random.Range(0, 10)]);
        }
    }

    public void AddAchievementToQueue(Achievement achievement)
    {
        AchievementsToShow.Enqueue(achievement);
    }
}
