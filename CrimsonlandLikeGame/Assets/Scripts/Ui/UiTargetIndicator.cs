﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiTargetIndicator : MonoBehaviour
{
    public Transform TargetTransform;

    public Image IndicatorImage;

    public RectTransform IndicatorRect;

    private float helperTimer = 0;

    public bool IsEnemy;

    // Update is called once per frame
    void Update()
    {
        if (TargetTransform != null)
        {
            Vector2 viewPortPoint = Camera.main.WorldToViewportPoint(TargetTransform.position);

            if (viewPortPoint.x > 0 && viewPortPoint.x < 1 && viewPortPoint.y > 0 && viewPortPoint.y < 1)
            {
                IndicatorImage.color = new Color(IndicatorImage.color.r, IndicatorImage.color.g, IndicatorImage.color.b, 0);
            }
            else
            {
                IndicatorImage.color = new Color(IndicatorImage.color.r, IndicatorImage.color.g, IndicatorImage.color.b, 1);

                float xAnchoredPosition = Mathf.Clamp(viewPortPoint.x, 0, 1);
                float xOffset = 0;
                if (xAnchoredPosition == 0) xOffset = IndicatorRect.sizeDelta.x / 2 + 10;
                if(xAnchoredPosition == 1 ) xOffset = -1 * (IndicatorRect.sizeDelta.x / 2 + 10);

                xAnchoredPosition *= UiGameCanvas.Get.GameCanvasRect.sizeDelta.x;
                xAnchoredPosition += xOffset;


                float yAnchoredPosition = Mathf.Clamp(viewPortPoint.y, 0, 1);
                float yOffset = 0;
                if (yAnchoredPosition == 0) yOffset = IndicatorRect.sizeDelta.x / 2 + 10;
                if (yAnchoredPosition == 1) yOffset = -1 * (IndicatorRect.sizeDelta.x / 2 + 10);

                yAnchoredPosition *= UiGameCanvas.Get.GameCanvasRect.sizeDelta.y;
                yAnchoredPosition += yOffset;

                IndicatorRect.anchoredPosition = new Vector2(xAnchoredPosition, yAnchoredPosition);
            }
        }
        else
        {
            IndicatorImage.color = new Color(IndicatorImage.color.r, IndicatorImage.color.g, IndicatorImage.color.b, 0);
        }

        helperTimer += Time.deltaTime;
        if (helperTimer > 10)
            gameObject.SetActive(false);
    }

    public void SetData(Transform targetTransform, bool isEnemy)
    {
        TargetTransform = targetTransform;
        IsEnemy = isEnemy;
        IndicatorImage.color = isEnemy ? UiIndicatorsController.Get.EnemyIndicatorColor : UiIndicatorsController.Get.NestIndicatorColor;
        IndicatorRect.sizeDelta = isEnemy ? new Vector2(25, 25) : new Vector2(45, 45);
    }

    public void OnDisable()
    {
        helperTimer = 0;
    }

}
