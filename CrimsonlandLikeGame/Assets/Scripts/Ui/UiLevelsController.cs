﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiLevelsController : MonoBehaviour
{
    public static UiLevelsController Get { get; private set; }

    public UiLevelsController()
    {
        Get = this;
    }

    public RectTransform ContentRect;

    public GameObject UiLevelPrefab;

    public List<UiLevel> UiLevels = new List<UiLevel>();

    // Use this for initialization
    void Start()
    {
        CreateAllLevelsButtons();
    }

    public void CreateAllLevelsButtons()
    {
        foreach(LevelPreset level in MainController.Get.Levels)
        {
            UiLevel uiLevel = Instantiate(UiLevelPrefab,ContentRect).GetComponent<UiLevel>();
            uiLevel.SetData(level);
            UiLevels.Add(uiLevel);
        }
    }

    public void LevelCompleted()
    {
        foreach(UiLevel level in UiLevels)
        {
            level.OnLevelCompleted();
        }
    }

    public void OnClickBackButton()
    {
        gameObject.SetActive(false);    
    }
}
