﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiHudController : MonoBehaviour
{

    public static UiHudController Get { get; set; }

    public UiHudController()
    {
        Get = this;
    }

    public Image HealthFill;

    public Image ArmorFill;

    public Image EnergyFill;

    public Text AmmoCountText;

    public Text TimeText;

    public CanvasGroup MainCanvasGroup;

    private bool fadingIn = false;
    private bool fadingOut = false;
    private float helperTimer;

    // Update is called once per frame
    void Update()
    {
        if (!MainController.LevelLoaded)
            return;

        if (PlayerController.Get != null)
        {
            float healthFillAmount = PlayerController.Get.Parameters.Health / PlayerController.Get.Parameters.GetMaxHealth();
            HealthFill.fillAmount = Mathf.Lerp(HealthFill.fillAmount, healthFillAmount, Time.deltaTime * 4);

            float armorFillAmount = PlayerController.Get.Parameters.Armor / 100;
            ArmorFill.fillAmount = Mathf.Lerp(ArmorFill.fillAmount, armorFillAmount, Time.deltaTime * 4);

            float energyFillAmount = PlayerController.Get.Parameters.Energy / 100;
            EnergyFill.fillAmount = Mathf.Lerp(EnergyFill.fillAmount, energyFillAmount, Time.deltaTime * 4);

        }

        if (fadingIn)
        {
            helperTimer += Time.deltaTime;

            MainCanvasGroup.alpha = Mathf.Lerp(0, 1, helperTimer / 0.5f);
            if (helperTimer >= 0.5f)
            {
                fadingIn = false;
            }
        }

        if (fadingOut)
        {
            helperTimer += Time.deltaTime;

            MainCanvasGroup.alpha = Mathf.Lerp(1, 0, helperTimer / 0.5f);
            if (helperTimer >= 0.5f)
            {
                fadingOut = false;
            }
        }

        AmmoCountText.text = GameEventsManager.Get.InfiniteAmmo ? "∞" : ((int)PlayerController.Get.WeaponsController.CurrentWeapon.AmmoCount).ToString();

        if (SurvivalModeController.Get != null)
        {
            TimeText.text = ((int)SurvivalModeController.Get.SurvivedTime).ToString();
        }
        else
        {
            TimeText.text = "";
        }
        
    }

    public void FadeIn()
    {
        helperTimer = 0;
        fadingIn = true;
    }

    public void FadeOut()
    {
        helperTimer = 0;
        fadingOut = true;
    }
}
