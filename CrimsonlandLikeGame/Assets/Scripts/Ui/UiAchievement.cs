﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class for single achievement in achievements list window
/// </summary>
public class UiAchievement : MonoBehaviour
{
    public Text TitleText;

    public Text DescriptionText;

    public Image BorderImage;

    public Text ValuesText;

    /// <summary>
    /// Sets data for this instance of ui achievement
    /// </summary>
    /// <param name="achievement"></param>
    public void SetData(Achievement achievement)
    {
        TitleText.text = achievement.AchievementName;
        DescriptionText.text = achievement.Description;
        ValuesText.text = achievement.CurrentValue + "/" + achievement.TargetValue;
        BorderImage.color = UiAchievementsList.Get.NotAchievedColor; //achievement.CurrentValue >= achievement.TargetValue ? UiAchievementsList.Get.AchievedColor : UiAchievementsList.Get.NotAchievedColor;
        TitleText.color = achievement.CurrentValue >= achievement.TargetValue ? UiAchievementsList.Get.AchievedColor : UiAchievementsList.Get.NotAchievedColor;
        ValuesText.color = achievement.CurrentValue >= achievement.TargetValue ? UiAchievementsList.Get.AchievedColor : UiAchievementsList.Get.NotAchievedColor;
    }

    /// <summary>
    /// Set this ui achievement as achieved, change colors of texts
    /// </summary>
    public void SetAchieved()
    {
        TitleText.color = UiAchievementsList.Get.AchievedColor;
        ValuesText.color = UiAchievementsList.Get.AchievedColor;
    }
}
