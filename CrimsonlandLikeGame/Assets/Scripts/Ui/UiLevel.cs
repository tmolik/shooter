﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiLevel : MonoBehaviour
{

    public LevelPreset RepresentedLevelPreset;

    public Text TitleText;

    public bool IsAvailable;


    public void SetData(LevelPreset level)
    {
        RepresentedLevelPreset = level;
        if (string.IsNullOrEmpty(level.LevelName))
            TitleText.text = level.name;
        else
            TitleText.text = level.LevelName;

        OnLevelCompleted();
    }

    public void OnClick()
    {
        if (!IsAvailable)
            return;

        MainController.Get.StartGameFromLevel(RepresentedLevelPreset);
    }

    public void OnLevelCompleted()
    {
        IsAvailable = RepresentedLevelPreset.ID <= GameStatsController.CurrentGameStats.LevelsCompleted+1;

        if (MainController.Get.Testing)
            IsAvailable = true;

        GetComponent<Button>().interactable = IsAvailable;
    }
}
