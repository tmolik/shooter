﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelPreset", menuName = "ScriptableObjects/LevelPreset", order = 2)]
public class LevelPreset : ScriptableObject
{
    public int ID;

    public List<EnemySpawnPreset> Enemies = new List<EnemySpawnPreset>();

    public List<SpiderNestPreset> SpidersNest = new List<SpiderNestPreset>();

    public int ExplosiveBarrelsCount;

    public int CampfiresCount;

    public int MinesCount;

    public string LevelName;

    public bool SpawnWeapons = true;

    public bool SpawnCollectables=true;

    public bool CanChangeWeapon = true;

    public LevelWeaponPreset WeaponPreset = new LevelWeaponPreset();


    public int GetAllEnemiesCount()
    {
        int sum = 0;
        foreach(EnemySpawnPreset preset in Enemies)
        {
            sum += preset.Count;
        }
        return sum;
    }

    public int GetAllNestsCount()
    {
        int sum = 0;
        foreach (SpiderNestPreset preset in SpidersNest)
        {
            sum += preset.Count;
        }
        return sum;
    }
}

[System.Serializable]
public class EnemySpawnPreset
{
    public GameObject EnemyToSpawn;
    public int Count;
}

[System.Serializable]
public class SpiderNestPreset
{
    public GameObject SpawnedEnemy;
    public int RespawnTime;
    public int Count;
}

[System.Serializable]
public class LevelWeaponPreset
{
    public bool ChangeWeapon;
    public EWeaponType WeaponType;
    public int AmmoCount;
}