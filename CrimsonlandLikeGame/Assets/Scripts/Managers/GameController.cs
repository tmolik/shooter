﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public static GameController Get { get; private set; }

    public GameController()
    {
        Get = this;
    }

    public List<HittableObject> Hittables = new List<HittableObject>();

    public int LevelsCount;

    public int CurrentLevel;

    public Light GameLight;

    public void Start()
    {
        GameObject.DontDestroyOnLoad(this);
    }

}
