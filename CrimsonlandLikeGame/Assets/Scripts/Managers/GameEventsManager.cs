﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventsManager : MonoBehaviour
{
    public static GameEventsManager Get { get; private set; }

    public GameEventsManager()
    {
        Get = this;
    }

    public bool EnemiesFrozen;
    private float enemiesFrozenTimer;

    public bool AttractingCollectables;
    private float attractingCollectablesTimer;

    public bool InfiniteAmmo = false;
    private float infiniteAmmoTimer = 0;

    public bool Blackout = false;
    private float blackoutHelperTimer = 0;
    private float BlackoutTime;

    public bool TimelinePlaying;

    // Update is called once per frame
    void Update()
    {
        if (EnemiesFrozen)
        {
            enemiesFrozenTimer += Time.deltaTime;
            if (enemiesFrozenTimer >= 5)
            {
                EnemiesFrozen = false;
            }
        }

        if (AttractingCollectables)
        {
            attractingCollectablesTimer += Time.deltaTime;
            if (attractingCollectablesTimer >= 10)
            {
                AttractingCollectables = false;
            }
        }

        if (InfiniteAmmo)
        {
            infiniteAmmoTimer += Time.deltaTime;

            if (infiniteAmmoTimer >= 10f)
            {
                InfiniteAmmo = false;
            }
        }

        if (Blackout)
        {
            blackoutHelperTimer += Time.deltaTime;

            if (blackoutHelperTimer >= BlackoutTime)
            {
                Blackout = false;
                GameController.Get.GameLight.enabled = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.B))
            StartBlackout(10);
    }

    public void StartAtractingCollectables()
    {
        AttractingCollectables = true;
        attractingCollectablesTimer = 0;
    }

    public void FreezeEnemies()
    {
        EnemiesFrozen = true;
        enemiesFrozenTimer = 0;
    }

    public void StartBlackout(float time)
    {
        BlackoutTime = time;
        blackoutHelperTimer = 0;
        Blackout = true;
        GameController.Get.GameLight.enabled = false;
    }


    public void StartInfiniteAmmo()
    {
        infiniteAmmoTimer = 0;
        InfiniteAmmo = true;
    }
}
