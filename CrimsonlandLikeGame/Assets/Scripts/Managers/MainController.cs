﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainController : MonoBehaviour
{
    public static MainController Get { get; set; }

    public static bool LevelLoaded = false;

    public static bool LoadingLevel = false;

    public static bool LoadedTrailerLevel = false;

    public List<LevelPreset> Levels = new List<LevelPreset>();

    public LevelPreset CurrentLevelPreset;

    private static string currentSceneName = "";

    public bool SlowTime;
    private float slowTimeHelperTimer;

    public static bool IsPreparingLevel = false;

    public bool Testing;

    public MainController()
    {
        Get = this;
    }

    // Use this for initialization
    void Start()
    {
        GameObject.DontDestroyOnLoad(this);
        SettingsManager.LoadSettings();
        StartCoroutine(LoadUiScene());
        GameStatsController.LoadStats();
        AchievementsController.LoadAchievements();
        PerksController.ResetAllPerks();
    }

    public void Update()
    {
        if (SlowTime)
        {
            slowTimeHelperTimer += Time.unscaledDeltaTime;
            if (slowTimeHelperTimer >= 10f)
            {
                SlowTime = false;
                Time.timeScale = 1;
            }
        }
    }

    public void SlowTimeStart()
    {
        SlowTime = true;
        slowTimeHelperTimer = 0;
        Time.timeScale = 0.4f;
    }
    public void OnApplicationQuit()
    {
        GameStatsController.SaveStats();
        AchievementsController.SaveAchievements();
        SettingsManager.SaveSettings();
    }


    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        Time.timeScale = SlowTime ? 0.4f : 1;

    }

    public IEnumerator LoadUiScene()
    {
        SceneManager.LoadSceneAsync("S_UI", LoadSceneMode.Additive);
        yield return new WaitForEndOfFrame();
    }

    public void StartGameFromLevel(LevelPreset level)
    {
        PlayerController.Get.Parameters.NewLevel();
        StartCoroutine(UnloadCurrentSceneAndLoadOther("Level_NewGame", false));
        GameController.Get.CurrentLevel = level.ID-1;
        StartCoroutine(PrepareAnotherLevel(false));
    }

    public void StartGame()
    {
        GameController.Get.CurrentLevel = 0;
        StartCoroutine(LoadLevel("Level_NewGame", false));
        StartCoroutine(PrepareAnotherLevel(false));
    }

    public void TestGame()
    {
        StartCoroutine(UnloadCurrentSceneAndLoadOther("Level_1_Bridge", true));
        CurrentLevelPreset = null;
        LoadedTrailerLevel = true;
    }

    public void SurvivalGame()
    {
        StartCoroutine(UnloadCurrentSceneAndLoadOther("Level_Survival", true));
        CurrentLevelPreset = null;

    }

    public void NextLevel()
    {
        PlayerController.Get.Parameters.NewLevel();

        if (GameStatsController.CurrentGameStats.LevelsCompleted < GameController.Get.CurrentLevel)
            GameStatsController.CurrentGameStats.LevelsCompleted = GameController.Get.CurrentLevel;

        UiLevelsController.Get.LevelCompleted();

        if (GameController.Get.CurrentLevel == Levels.Count)
            Debug.Log("Ostatni poziom");
        else
            StartCoroutine(PrepareAnotherLevel(true));
    }

    private IEnumerator PrepareAnotherLevel(bool showNotification)
    {
        Debug.Log("Doszlo tu");


        while (!LevelLoaded)
        {
            Debug.Log("Ładuje jeszcze poziom więc czekam");
            yield return null;
        }

        if (GameController.Get.CurrentLevel > 0 && showNotification)
        {
            UiNotificationsController.Get.ShowNotification("Level " + GameController.Get.CurrentLevel + " completed!");
            yield return new WaitForSecondsRealtime(2.2f);
        }
        Debug.Log("Załadowało poziom");

        UiLoadingScreenController.Get.FadeIn();
        yield return new WaitForSecondsRealtime(1f);
        Debug.Log("Poczekało sekunde by fadein loading screen");

        ClearPreviousLevel();
        //yield return new WaitForSeconds(1f);
        Debug.Log("Oczyscilo poziom");

        if (GameController.Get.CurrentLevel == Levels.Count)
        {
            Debug.Log("That was last level");
        }
        else
        {
            GameController.Get.CurrentLevel++;
            PrepareLevel(Levels[GameController.Get.CurrentLevel - 1]);
            CurrentLevelPreset = Levels[GameController.Get.CurrentLevel - 1];
        }
        Debug.Log("Doszlo tu 3");

        while (IsPreparingLevel)
        {
            Debug.Log("PRzygotowuje poziom więc czekam");

            yield return null;
        }

        Debug.Log("Doszlo tu 4");

        FinishLoading();

        yield return new WaitForSecondsRealtime(3f);
        LevelInfo.Get.LoadedNextLevel = false;

    }

    public void FinishLoading()
    {
        UIMainMenuController.Get.FadeOut();
        UiHudController.Get.FadeIn();

        PlayerController.Get.transform.position = Vector3.zero + Vector3.up * 2;
        PlayerController.Get.GetComponent<Rigidbody>().velocity = Vector3.zero;
        PlayerController.Get.GetComponent<Rigidbody>().useGravity = true;
        LoadingLevel = false;

        UiLoadingScreenController.Get.FadeOut();
    }

    private IEnumerator UnloadCurrentSceneAndLoadOther(string sceneToLoad, bool finishLoading)
    {
        LevelLoaded = false;
        UiLoadingScreenController.Get.FadeIn();
        if (currentSceneName != "")
        {
            AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(currentSceneName);
            while (!asyncUnload.isDone)
            {
                yield return null;
            }
        }
        StartCoroutine(LoadLevel(sceneToLoad, finishLoading));
    }

    /// <summary>
    /// Loads level of given name
    /// </summary>
    /// <param name="levelName"></param>
    /// <returns></returns>
    private IEnumerator LoadLevel(string levelName, bool finishLoading)
    {
        if (LoadingLevel)
            yield break;

        PlayerController.Get.GetComponent<Rigidbody>().useGravity = false;

        LoadingLevel = true;
        AsyncOperation asyncload = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);

        // wait until the asynchronous scene fully loads
        while (!asyncload.isDone)
        {
            yield return null;
        }
        currentSceneName = levelName;
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(levelName));

        LevelLoaded = true;
        if (finishLoading)
            FinishLoading();
    }

    public void GameOver()
    {
        StartCoroutine(GameOverCoroutine());
    }

    private IEnumerator GameOverCoroutine()
    {

        yield return new WaitForSecondsRealtime(2);

        /* Show game over notification */
        UiNotificationsController.Get.ShowNotification("Game over!");
        yield return new WaitForSecondsRealtime(2.2f);

        if (currentSceneName != "")
        {
            AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(currentSceneName);
            while (!asyncUnload.isDone)
            {
                yield return null;
            }
            currentSceneName = "";
        }
        LevelLoaded = false;
        PlayerController.Get.ResetPlayer();
        UIMainMenuController.Get.FadeIn();

    }

    public void ClearPreviousLevel()
    {
        for (int i = LevelInfo.Get.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(LevelInfo.Get.transform.GetChild(i).gameObject);
        }

        CollectablesManager.Get.ClearCollectables();
    }

    public void PrepareLevel(LevelPreset preset)
    {
        IsPreparingLevel = true;
        foreach (EnemySpawnPreset spawnPreset in preset.Enemies)
        {
            if (spawnPreset.EnemyToSpawn != null)
            {
                for (int i = 0; i < spawnPreset.Count; i++)
                {
                    EnemyController enemy = Instantiate(spawnPreset.EnemyToSpawn).GetComponent<EnemyController>();
                    enemy.transform.SetParent(LevelInfo.Get.transform);
                    enemy.transform.position = GetRandomPosition();
                }
            }
        }

        foreach (SpiderNestPreset nestPreset in preset.SpidersNest)
        {
            if (nestPreset.SpawnedEnemy != null)
            {
                for (int i = 0; i < nestPreset.Count; i++)
                {
                    EnemySpawner spawner = Instantiate(PrefabsHelper.Get.EnemySpawnerPrefab).GetComponent<EnemySpawner>();
                    spawner.transform.position = GetRandomPosition();
                    spawner.RespawnTime = nestPreset.RespawnTime;
                    if (spawner.EnemiesToRespawn == null)
                        spawner.EnemiesToRespawn = new List<GameObject>();
                    spawner.EnemiesToRespawn.Add(nestPreset.SpawnedEnemy);
                    spawner.transform.SetParent(LevelInfo.Get.transform);

                }
            }
        }

        for (int i = 0; i < preset.ExplosiveBarrelsCount; i++)
        {
            GameObject barrel = Instantiate(PrefabsHelper.Get.BarrelPrefab);
            barrel.transform.position = GetRandomPosition();
            barrel.transform.SetParent(LevelInfo.Get.transform);
        }

        for (int i = 0; i < preset.MinesCount; i++)
        {
            GameObject mine = ObjectPooler.Get.GetPooledObject("Mine");
            mine.transform.position = GetRandomPosition();
            mine.transform.SetParent(LevelInfo.Get.transform);
            mine.gameObject.SetActive(true);
        }

        for (int i = 0; i < preset.CampfiresCount; i++)
        {
            GameObject campfire = Instantiate(PrefabsHelper.Get.CampfirePrefab,GetRandomPosition(),Quaternion.Euler(-90,0,0));
            campfire.transform.SetParent(LevelInfo.Get.transform);

        }

        if (preset.WeaponPreset.ChangeWeapon)
        {
            PlayerController.Get.WeaponsController.Reset();
            PlayerController.Get.WeaponsController.ChangeWeapon(preset.WeaponPreset.WeaponType, preset.WeaponPreset.AmmoCount, true);
        }
        IsPreparingLevel = false;

    }

    public Vector3 GetRandomPosition()
    {
        int multiplier = Random.Range(0, 100) > 50 ? -1 : 1;
        int multiplier2 = Random.Range(0, 100) > 50 ? -1 : 1;
        return new Vector3(Random.Range(10, 50) * multiplier, 0, Random.Range(10, 50) * multiplier2);
    }
}
