﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Game settings class
/// </summary>
public class Settings
{
    public float MasterVolume = 1f;

    public bool MusicOn = true;
}
