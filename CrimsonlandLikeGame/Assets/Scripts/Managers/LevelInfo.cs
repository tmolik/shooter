﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInfo : MonoBehaviour
{
    public static LevelInfo Get { get; set; }

    public LevelInfo()
    {
        Get = this;
    }

    public EGameMode GameMode;

    /// <summary> List of enemies that are currently spawned in the game </summary>
    public List<EnemyController> Enemies = new List<EnemyController>();

    public List<EnemySpawner> Spawners = new List<EnemySpawner>();

    public bool LoadedNextLevel = false;

    public int DestroyEnemiesAfter = 240;

    public void Start()
    {
        if (GameMode == EGameMode.Normal)
            InvokeRepeating("Tick", 10, 1);
    }

    public void Tick()
    {
        if (Enemies.Count == 0 && Spawners.Count == 0 && !LoadedNextLevel && MainController.LevelLoaded)
        {
            Debug.Log("Wow, skonczony poziom");
            MainController.Get.NextLevel();
            LoadedNextLevel = true;
        }
    }
}
