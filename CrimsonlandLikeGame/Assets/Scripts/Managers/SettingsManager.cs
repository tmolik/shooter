﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class SettingsManager
{
    public static Settings CurrentSettings;

    public const string FileName = "Settings.json";

    public static string FilePath { get { return Application.persistentDataPath + "/" + FileName; } }


    public static void SaveSettings()
    {
        if (CurrentSettings == null)
        {
            LoadSettings();
        }
        StreamWriter file = new StreamWriter(FilePath, false);
        file.Write(JsonUtility.ToJson(CurrentSettings));
        file.Close();
    }

    public static void LoadSettings()
    {
        try
        {
            string jsonSettings = File.ReadAllText(FilePath);
            if (string.IsNullOrEmpty(jsonSettings))
            {
                Debug.Log("File was empty or nulled");
                CurrentSettings = new Settings();
            }
            else
            {
                Settings settingsFromFile = JsonUtility.FromJson<Settings>(jsonSettings);
                CurrentSettings = settingsFromFile;
                Debug.Log("Succesfully loaded settings from file");
            }
        }
        catch (Exception e)
        {
            CurrentSettings = new Settings();
            Debug.Log("Something went wrong when reading file : " + e.Message);
            return;
        }


        ChangedMasterVolumeSlider(CurrentSettings.MasterVolume);
    }

    public static void ChangedMasterVolumeSlider(float volume)
    {
        AudioListener.volume = volume;
        CurrentSettings.MasterVolume = volume;
    }
}
