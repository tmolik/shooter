﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurvivalModeController : MonoBehaviour
{
    public static SurvivalModeController Get { get; private set; }

    public SurvivalModeController()
    {
        Get = this;
    }


    public float SurvivedTime;

    public int EnemiesToSpawnCount;
    public int SpawnEnemiesTime;

    private float helperTimer;

    // Use this for initialization
    void Start()
    {
        SpawnEnemiesTime = 10;
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerController.Get.Parameters.IsAlive)
            return;

        SurvivedTime += Time.deltaTime;

        helperTimer += Time.deltaTime;
        if (helperTimer >= SpawnEnemiesTime)
        {
            SpawnEnemies();
            helperTimer = 0;
            SpawnExplosives();
        }
    }

    public void SpawnEnemies()
    {
        for (int i = 0; i < GetSpawnCount(); i++)
        {
            GameObject enemy = Instantiate(GetSpider());
            enemy.transform.position = CollectablesManager.Get.transform.position + new Vector3(Random.Range(-30, 30), 0, Random.Range(-30, 30));
        }
    }

    public void SpawnExplosives()
    {
        int barrelsCount = Random.Range(3, 6);
        int minesCount = Random.Range(2, 5);

        for(int i=0; i<barrelsCount; i++)
        {
            GameObject barrel = Instantiate(PrefabsHelper.Get.BarrelPrefab);
            barrel.transform.position = MainController.Get.GetRandomPosition();
        }

        for (int i = 0; i < minesCount; i++)
        {
            GameObject mine = Instantiate(PrefabsHelper.Get.MinePrefab);
            mine.transform.position = MainController.Get.GetRandomPosition();
        }
    }

    public int GetSpawnCount()
    {
        return (int)SurvivedTime / 15 + 1;
    }

    public GameObject GetSpider()
    {
        int rand = Random.Range(0, 100);

        if (rand <= 90)
        {
            int index = (int)SurvivedTime / 60;
            if (index > PrefabsHelper.Get.AllSpiders.Count - 1) index = PrefabsHelper.Get.AllSpiders.Count - 1;

            index += Random.Range(-4, 4);
            if (index < 0) index = 0;
            if (index > PrefabsHelper.Get.AllSpiders.Count - 1) index = PrefabsHelper.Get.AllSpiders.Count - 1;
            return PrefabsHelper.Get.AllSpiders[index];
        }
        else
            return PrefabsHelper.Get.SpiderKamikaze;
    }
}

