﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Collectable that gives player armor when collected
/// </summary>
public class Collectable_Armor : CollectableObjectController
{
    /// <summary> How much armor will be added</summary>
    public float ArmorToAdd;

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();
        PlayerController.Get.Parameters.Armor = Mathf.Min(PlayerController.Get.Parameters.Armor + ArmorToAdd, 100);
        DestroyCollectable();
    }

}
