﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Collectable that increases player movement speed
/// </summary>
public class Collectable_MoveSpeed : CollectableObjectController
{
    /// <summary> How much will speed increase</summary>
    public float SpeedBonus;

    /// <summary> How long will speed bonus last</summary>
    public float BonusTime;

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();
        PlayerController.Get.Parameters.AddSpeedBonus(SpeedBonus, BonusTime);
        DestroyCollectable();
    }


}
