﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_Shield : CollectableObjectController
{

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();

        PlayerController.Get.Parameters.SetShield();

        DestroyCollectable();
    }
}
