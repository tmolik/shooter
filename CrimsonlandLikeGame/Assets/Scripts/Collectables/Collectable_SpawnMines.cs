﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_SpawnMines : CollectableObjectController {


    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();
        
        for(int i = 0; i<10; i++)
        {
            GameObject mine = ObjectPooler.Get.GetPooledObject("Mine");
            mine.transform.position = MainController.Get.GetRandomPosition();
            mine.transform.SetParent(LevelInfo.Get.transform);
            mine.gameObject.SetActive(true);
        }

        DestroyCollectable();
    }
}
