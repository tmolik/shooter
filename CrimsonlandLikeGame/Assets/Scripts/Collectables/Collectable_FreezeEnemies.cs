﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_FreezeEnemies : CollectableObjectController
{

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();
        GameEventsManager.Get.FreezeEnemies();
        DestroyCollectable();
    }
}
