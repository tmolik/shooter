﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_SlowTime : CollectableObjectController
{

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();
        MainController.Get.SlowTimeStart();
        DestroyCollectable();
    }

    public override void PlayOnCollectedSound()
    {
        AudioManager.Get.PlayCollectedSlowTime(transform.position);
    }
}
