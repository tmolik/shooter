﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for every collectable
/// </summary>
public class CollectableObjectController : MonoBehaviour
{
    /// <summary> How much time will this collectable be on map, then it dissapears </summary>
    public float LifeTime;

    /// <summary> Particle spawned when particle is collected </summary>
    public GameObject OnCollectedParticle;

    /// <summary> Light attached to this collectable </summary>
    public Light AttachedLight;

    public bool IsWeapon;

    /// <summary> Attached light intensity at start </summary>
    private float startingLightIntensity;

    private float helperTimer;
    private float randomizer;
    private float rotationRandomizer;

    /// <summary> Is collectable shrinking after collected </summary>
    private bool shrinking = false;
    private float shrinkingTimer = 0;

    /// <summary> Is collectable growing (after creation) </summary>
    private bool growing = false;
    private float growingTimer = 0;

    public void Start()
    {
        randomizer = Random.Range(1, 6);
        rotationRandomizer = Random.Range(1f, 3f);
        growing = true;
        transform.localScale = Vector3.zero;

        if (AttachedLight != null)
            startingLightIntensity = AttachedLight.intensity;
    }

    // Update is called once per frame
    public void Update()
    {
        helperTimer += Time.deltaTime;

        if (helperTimer >= LifeTime)
        {
            DestroyCollectable();
        }

        if (GameEventsManager.Get.AttractingCollectables)
        {
            float distanceFromPlayer = Vector3.Distance(transform.position, PlayerController.Get.transform.position);

            if (distanceFromPlayer < 5)
            {
                transform.position += (PlayerController.Get.transform.position - transform.position).normalized * 6 * Time.deltaTime;
            }
        }

        float xValue = Mathf.Sin(randomizer + Time.time * 2) * 0.3f * Time.deltaTime;
        float zValue = Mathf.Sin(randomizer * 2 + Time.time * 2) * 0.3f * Time.deltaTime;
        transform.position += new Vector3(xValue, 0, zValue);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, transform.rotation.eulerAngles.y + 90, IsWeapon ? 90 : 0), rotationRandomizer);


        if (growing)
        {
            growingTimer += Time.deltaTime;
            float easingValue = EasingFunction.EaseInOutCirc(0, 1, growingTimer / 0.7f);

            transform.localScale = Vector3.LerpUnclamped(Vector3.zero, Vector3.one, easingValue);

            if (growingTimer >= 0.7f)
                growing = false;
        }


        if (shrinking)
        {
            shrinkingTimer += Time.deltaTime;
            float easingValue = EasingFunction.EaseInOutCirc(0, 1, shrinkingTimer / 0.7f);
            transform.localScale = Vector3.LerpUnclamped(Vector3.one, Vector3.zero, easingValue);

            if (AttachedLight != null)
                AttachedLight.intensity = Mathf.Lerp(startingLightIntensity, 0, easingValue);

            if (shrinkingTimer >= 0.7f)
                Destroy(gameObject);
        }


    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == "Player")
        {
            OnCollectedByPlayer();
        }
    }


    public virtual void OnCollectedByPlayer()
    {
        if (OnCollectedParticle != null)
        {
            GameObject particle = Instantiate(OnCollectedParticle);
            particle.transform.position = transform.position;
        }
        GameStatsController.CollectableTaken();
        PlayOnCollectedSound();
    }

    public virtual void PlayOnCollectedSound()
    {
        AudioManager.Get.PlayCollectedPill(transform.position);
    }

    /// <summary>
    /// Animates and destroys this collectable
    /// </summary>
    public void DestroyCollectable()
    {
        Collider collider = GetComponent<Collider>();
        if (collider != null)
            collider.enabled = false;

        if (!shrinking)
        {
            shrinkingTimer = 0;
            shrinking = true;
        }
    }

}
