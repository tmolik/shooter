﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_DontUseAmmo : CollectableObjectController
{

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();
        GameEventsManager.Get.StartInfiniteAmmo();

        DestroyCollectable();
    }
}
