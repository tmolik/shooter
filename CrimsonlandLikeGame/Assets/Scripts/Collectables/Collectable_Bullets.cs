﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_Bullets : CollectableObjectController
{

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();

        for (int i = 0; i < 8; i++)
        {
            BulletController bullet = ObjectPooler.Get.GetPooledObject("BulletCollectable").GetComponent<BulletController>();

            if (bullet == null)
                return;

            bullet.transform.rotation = transform.rotation;
            bullet.Direction = GetDirectionForBullet(i);
            bullet.transform.position = transform.position + bullet.Direction;

            bullet.Damage = 20;
            bullet.gameObject.SetActive(true);
        }

        DestroyCollectable();
    }

    private Vector3 GetDirectionForBullet(int index)
    {
        switch (index)
        {
            case 0: return transform.forward;
            case 1: return transform.forward + transform.right;
            case 2: return transform.right;
            case 3: return transform.forward * -1 + transform.right;
            case 4: return transform.forward * -1;
            case 5: return transform.forward *-1 + transform.right * -1;
            case 6: return transform.right * -1;
            case 7: return transform.right * -1 + transform.forward;
        }
        return transform.forward;
    }
}
