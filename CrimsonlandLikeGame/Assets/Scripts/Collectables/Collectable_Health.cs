﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Collectble that adds health when collected
/// </summary>
public class Collectable_Health : CollectableObjectController
{
    /// <summary> How much health will this collectable restore </summary>
    public float HealthToAdd;

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();
        PlayerController.Get.Parameters.Health = Mathf.Min(PlayerController.Get.Parameters.Health + HealthToAdd,PlayerController.Get.Parameters.GetMaxHealth());
        DestroyCollectable();
    }
}
