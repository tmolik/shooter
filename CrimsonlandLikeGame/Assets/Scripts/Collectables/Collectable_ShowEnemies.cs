﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_ShowEnemies : CollectableObjectController
{

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();

        UiIndicatorsController.Get.ShowIndicators();

        DestroyCollectable();
    }

}
