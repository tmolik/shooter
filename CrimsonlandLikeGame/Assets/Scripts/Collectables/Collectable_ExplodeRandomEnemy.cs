﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_ExplodeRandomEnemy : CollectableObjectController
{

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();

        EnemyController enemy = LevelInfo.Get.Enemies[Random.Range(0, LevelInfo.Get.Enemies.Count)];

        enemy.OnHit(1000);

        GameObject explode = ObjectPooler.Get.GetPooledObject("Explode");

        explode.transform.position = enemy.transform.position;
        explode.gameObject.SetActive(true);
        explode.GetComponent<Explosion>().Explode();

        DestroyCollectable();
    }
}
