﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manager for collectables, spawning and stuff
/// </summary>
public class CollectablesManager : MonoBehaviour
{
    public static CollectablesManager Get { get; private set; }

    public CollectablesManager()
    {
        Get = this;
    }


    /// <summary> Time to spawn next collectable </summary>
    public float SpawnTime;

    private float helperTimer;



    // Use this for initialization
    void Start()
    {
        SpawnTime = Random.Range(2, 5);
        GameObject.DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        helperTimer += Time.deltaTime;

        if (helperTimer >= SpawnTime)
        {
            helperTimer = 0;
            SpawnCollectable(MainController.Get.GetRandomPosition());
            SpawnTime = Random.Range(2, 5);
            SpawnTime = SpawnTime - ((PerksController.Perks[EPerkType.CollectablesSpawnTime].CurrentValue / 100) * SpawnTime);
        }

    }

    /// <summary>
    /// Spawning random collectable and placing it somewhere on the map
    /// </summary>
    public void SpawnCollectable(Vector3 position)
    {
        if (LevelInfo.Get != null && LevelInfo.Get.GameMode == EGameMode.Campaign)
            return;


        bool spawnWeapons =true;
        bool spawnCollectables =true;
        if (MainController.Get.CurrentLevelPreset!=null)
        {
            spawnWeapons = MainController.Get.CurrentLevelPreset.SpawnWeapons;
            spawnCollectables = MainController.Get.CurrentLevelPreset.SpawnCollectables;
        }

        if (!spawnCollectables && !spawnWeapons)
            return;

        GameObject collectable = null;

        if (spawnWeapons && spawnCollectables)
        {
            if (Random.Range(0, 100) > 80)
                collectable = Instantiate(PrefabsHelper.Get.GetCollectablePrefab(ECollectableType.Weapon), transform);
            else
                collectable = Instantiate(PrefabsHelper.Get.GetRandomCollectablePrefab(), transform);
        }
        else if (spawnWeapons)
        {
            collectable = Instantiate(PrefabsHelper.Get.GetCollectablePrefab(ECollectableType.Weapon), transform);
        }
        else if (spawnCollectables)
        {
            collectable = Instantiate(PrefabsHelper.Get.GetRandomCollectablePrefab(), transform);
        }

        collectable.transform.position = position;
    }

    public void ClearCollectables()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }
}
