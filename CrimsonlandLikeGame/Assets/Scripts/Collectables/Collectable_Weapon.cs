﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Collectable weapon
/// </summary>
public class Collectable_Weapon : CollectableObjectController
{
    /// <summary> What weapon will be given when collected</summary>
    public EWeaponType WeaponType;

    public int AmmoCount;

    // Use this for initialization
    new void Start()
    {
        base.Start();
        Array v = Enum.GetValues(typeof(EWeaponType));
        WeaponType = (EWeaponType)v.GetValue(UnityEngine.Random.Range(1, v.Length));

        GameObject weapon = Instantiate(PrefabsHelper.Get.GetWeaponPrefab(WeaponType), transform);
        weapon.transform.localPosition = Vector3.zero;

        AmmoCount = UnityEngine.Random.Range(20, 30);

    }

    public new void Update()
    {
        base.Update();

        Quaternion newRotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, 40, 0) * Time.deltaTime);
        transform.rotation = newRotation;
    }

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();
        
        int newAmmoCount = AmmoCount + (int)(PerksController.Perks[EPerkType.CollectMoreAmmo].CurrentValue / 100 * AmmoCount);
        PlayerController.Get.WeaponsController.ChangeWeapon(WeaponType, newAmmoCount);
        DestroyCollectable();
    }

    public override void PlayOnCollectedSound()
    {
        AudioManager.Get.PlayCollectedWeapon(transform.position);
    }
}
