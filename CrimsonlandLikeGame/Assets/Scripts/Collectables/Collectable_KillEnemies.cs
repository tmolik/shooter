﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_KillEnemies : CollectableObjectController
{
    /// <summary> Enemies in that range will be killed </summary>
    public float Range;

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();

        LevelInfo level = LevelInfo.Get;
        if (level != null)
        {
            List<EnemyController> enemiesCopy = new List<EnemyController>(level.Enemies);
            foreach (EnemyController enemy in enemiesCopy)
            {
                if (Vector3.Distance(enemy.transform.position, transform.position) < Range)
                {
                    if (enemy.Health > 0)
                        enemy.OnHit(300);
                }
            }
        }

        KillAllEfect killAll = Instantiate(PrefabsHelper.Get.KillAllPrefab).GetComponent<KillAllEfect>();
        killAll.transform.position = transform.position;
        killAll.Size =(int) Range;

        DestroyCollectable();
    }
}
