﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_AttractingCollectables : CollectableObjectController
{

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();

        GameEventsManager.Get.StartAtractingCollectables();

        DestroyCollectable();
    }
}
