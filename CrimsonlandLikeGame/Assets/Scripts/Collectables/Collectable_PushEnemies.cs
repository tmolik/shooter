﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Collectable that when collected, pushes away enemies that are close
/// </summary>
public class Collectable_PushEnemies : CollectableObjectController
{
    /// <summary> Enemies in that range will be pushed away </summary>
    public float Range;

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();
        LevelInfo level = LevelInfo.Get;
        if (level != null)
        {
            foreach (EnemyController enemy in level.Enemies)
            {
                if (Vector3.Distance(enemy.transform.position, transform.position) < Range)
                {
                    enemy.Push(transform.position);
                }
            }
        }
        DestroyCollectable();
    }
}
