﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable_SetAllEnemiesOnFire : CollectableObjectController
{

    public override void OnCollectedByPlayer()
    {
        base.OnCollectedByPlayer();

        foreach(EnemyController enemy in LevelInfo.Get.Enemies)
        {
            enemy.SetOnFire();
        }

        DestroyCollectable();   
    }

}
