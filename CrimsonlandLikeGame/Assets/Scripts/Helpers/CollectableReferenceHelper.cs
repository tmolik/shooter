﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple helper class to reference collectables and show it in list in inspector
/// </summary>
[System.Serializable]
public class CollectableReferenceHelper
{
    public ECollectableType CollectableType;
    public GameObject Collectable;
}
