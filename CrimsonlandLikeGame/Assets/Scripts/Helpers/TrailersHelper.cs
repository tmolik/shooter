﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailersHelper : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            PlayerController.Get.WeaponsController.ChangeWeapon(EWeaponType.Pistol, 100);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            PlayerController.Get.WeaponsController.ChangeWeapon(EWeaponType.Uzi, 100);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            PlayerController.Get.WeaponsController.ChangeWeapon(EWeaponType.Shotgun, 99);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            PlayerController.Get.WeaponsController.ChangeWeapon(EWeaponType.RocketLauncher, 100);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            PlayerController.Get.WeaponsController.ChangeWeapon(EWeaponType.Sniper, 100);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            PlayerController.Get.WeaponsController.ChangeWeapon(EWeaponType.Flamethrower, 100);
        }

        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            SpawnBarrel();
        }

        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            SpawnNest();
        }

        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            CollectablesManager.Get.SpawnCollectable(MainController.Get.GetRandomPosition());
        }

        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            SpawnSpider(0);
        }

        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            SpawnSpider(1);
        }

        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            SpawnSpider(2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            SpawnSpider(3);
        }
        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            SpawnSpider(4);
        }

        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            SpawnSpider(5);
        }
        if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            SpawnSpider(6);
        }

        if (Input.GetKeyDown(KeyCode.Keypad7))
        {
            SpawnSpider(7);
        }
        if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            SpawnSpider(8);
        }

        if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            SpawnSpider(9);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (Time.timeScale == 1)
                MainController.Get.PauseGame();
            else
                MainController.Get.UnpauseGame();
        }
    }

    public void SpawnSpider(int index)
    {
        if (PrefabsHelper.Get.AllSpiders.Count-1 > index )
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit, 1000, PlayerController.Get.RaycastLayerMask, QueryTriggerInteraction.Collide))
            {
                if (raycastHit.collider.gameObject.name != "PlayerTouchCollider")
                {
                    Vector3 pos = new Vector3(raycastHit.point.x, 0, raycastHit.point.z);
                    GameObject spider = Instantiate(PrefabsHelper.Get.AllSpiders[index]);
                    spider.transform.position = pos;
                }
            }
        }
    }

    public void SpawnBarrel()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, 1000, PlayerController.Get.RaycastLayerMask, QueryTriggerInteraction.Collide))
        {
            if (raycastHit.collider.gameObject.name != "PlayerTouchCollider")
            {
                Vector3 pos = new Vector3(raycastHit.point.x, 0, raycastHit.point.z);
                GameObject spider = Instantiate(PrefabsHelper.Get.BarrelPrefab);
                spider.transform.position = pos;
            }
        }
    }

    public void SpawnNest()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, 1000, PlayerController.Get.RaycastLayerMask, QueryTriggerInteraction.Collide))
        {
            if (raycastHit.collider.gameObject.name != "PlayerTouchCollider")
            {
                Vector3 pos = new Vector3(raycastHit.point.x, 0, raycastHit.point.z);
                GameObject spider = Instantiate(PrefabsHelper.Get.EnemySpawnerPrefab);
                spider.GetComponent<EnemySpawner>().EnemiesToRespawn = new List<GameObject> { PrefabsHelper.Get.AllSpiders[Random.Range(0, PrefabsHelper.Get.AllSpiders.Count)] };
                spider.transform.position = pos;
            }
        }
    }
}
