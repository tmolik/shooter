﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class to control crosshairs
/// </summary>
public class CrosshairController : MonoBehaviour
{
    public static CrosshairController Get { get; set; }

    public CrosshairController()
    {
        Get = this;
    }

    /// <summary> List of crosshair helpers </summary>
    public List<CrosshairHelper> Crosshairs = new List<CrosshairHelper>();

    public Image CrosshairImage;

    public void Start()
    {
        SwitchToCursor();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        CrosshairImage.transform.position = mousePos;
    }

    /// <summary>
    /// Sets crosshair for given weapon
    /// </summary>
    /// <param name="weaponType"></param>
    public void SwitchCrosshair(EWeaponType weaponType)
    {
        foreach (CrosshairHelper crosshair in Crosshairs)
        {
            if (crosshair.WeaponType == weaponType)
            {
                CrosshairImage.sprite = crosshair.CrosshairSprite;
                break;
            }
        }
    }

    /// <summary>
    /// Sets crooshair invisible and cursor visible
    /// </summary>
    public void SwitchToCursor()
    {
        Cursor.visible = true;
        CrosshairImage.color = new Color(CrosshairImage.color.r, CrosshairImage.color.g, CrosshairImage.color.b, 0);
    }

    /// <summary>
    /// Sets crooshair visible and cursor invisible
    /// </summary>
    public void SwitchToCrosshair()
    {
        Cursor.visible = false;
        CrosshairImage.color = new Color(CrosshairImage.color.r, CrosshairImage.color.g, CrosshairImage.color.b, 1);
    }
}

/// <summary>
/// Simple helper class to link crosshair sprite with weapon type
/// </summary>
[System.Serializable]
public class CrosshairHelper
{
    public EWeaponType WeaponType;
    public Sprite CrosshairSprite;
}