﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Helper class to assign some prefabs
/// </summary>
public class PrefabsHelper : MonoBehaviour
{
    public static PrefabsHelper Get { get; private set; }

    public PrefabsHelper()
    {
        Get = this;
    }

    public GameObject BarrelPrefab;

    public GameObject MinePrefab;

    public GameObject BloodPrefab;

    public GameObject GrenadeExplode;

    public GameObject BarrelExplode;

    public GameObject CampfirePrefab;

    public GameObject SpiderExplode;

    public GameObject SpiderNestDestroy;

    public GameObject BloodSplatPrefab;

    public GameObject EnemySpawnerPrefab;

    public GameObject DestroyedSpiderNest;

    public GameObject KillAllPrefab;

    public List<GameObject> AllSpiders = new List<GameObject>();

    public GameObject SpiderSpawnCreator;

    public GameObject SpiderKamikaze;

    public GameObject SpiderMother;

    public GameObject SpiderMotherDefender;

    public GameObject UIDialogueBoxPrefab;

    /// <summary> List of weapons references </summary>
    public List<WeaponReferenceHelper> Weapons = new List<WeaponReferenceHelper>();

    /// <summary> List of collectables references </summary>
    public List<CollectableReferenceHelper> Collectables = new List<CollectableReferenceHelper>();


    public void Start()
    {
        GameObject.DontDestroyOnLoad(this);
        
    }

    /// <summary>
    /// Returns prefab of weapon of given type
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public GameObject GetWeaponPrefab(EWeaponType type)
    {
        foreach(WeaponReferenceHelper weaponPrefabHelper in Weapons)
        {
            if (weaponPrefabHelper.WeaponType == type)
                return weaponPrefabHelper.WeaponGameobject;
        }

        return null;
    }

    /// <summary>
    /// Returns prefab of collectable of given type
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public GameObject GetCollectablePrefab(ECollectableType type)
    {
        foreach (CollectableReferenceHelper collectablePrefabHelper in Collectables)
        {
            if (collectablePrefabHelper.CollectableType == type)
                return collectablePrefabHelper.Collectable;
        }

        return null;
    }

    /// <summary>
    /// Returns random collectable prefab
    /// </summary>
    /// <returns></returns>
    public GameObject GetRandomCollectablePrefab()
    {
        return Collectables[Random.Range(0, Collectables.Count)].Collectable;
    }
}
