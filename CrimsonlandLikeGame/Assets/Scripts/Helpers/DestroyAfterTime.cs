﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
    public float TimeToDestroy;

    private float helperTimer;

    // Use this for initialization
    void Start()
    {
        helperTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        helperTimer += Time.deltaTime;
        if (helperTimer >= TimeToDestroy)
            Destroy(gameObject);
    }
}
