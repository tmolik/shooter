﻿using UnityEngine;

/// <summary>
/// Simple helper class to reference weapons and show it in list in inspector
/// </summary>
[System.Serializable]
public class WeaponReferenceHelper
{
    public EWeaponType WeaponType;
    public GameObject WeaponGameobject;
}
