﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioClipsCollection", menuName = "ScriptableObjects/AudioClipsCollection", order = 1)]
public class AudioClipsCollection : ScriptableObject
{
    public List<AudioClip> Clips = new List<AudioClip>();

    public AudioClip GetRandomAudioClip()
    {
        return Clips[Random.Range(0, Clips.Count)];
    }
   
}
