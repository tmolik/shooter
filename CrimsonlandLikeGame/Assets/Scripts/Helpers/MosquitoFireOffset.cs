﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MosquitoFireOffset : FireOffset
{
    public EnemyController AttachedToEnemy;

    public Vector3 StartOffset;

    public void Start()
    {
        StartOffset = Offset;
        AttachedToEnemy = GetComponent<EnemyController>();
    }

    public void Update()
    {
        if (AttachedToEnemy != null)
        {
            if (!AttachedToEnemy.IsAlive)
            {
                //Debug.Log("Czy isname Death? " + (AttachedToEnemy.EnemyAnimator.GetCurrentAnimatorStateInfo(0).IsName("Death")));
                if (AttachedToEnemy.EnemyAnimator.GetCurrentAnimatorStateInfo(0).IsName("Death"))
                {
                    Offset = Vector3.Lerp(StartOffset, Vector3.zero, AttachedToEnemy.EnemyAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime);
                }
            }
        }
    }
}
