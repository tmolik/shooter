﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendererSwitcher : MonoBehaviour
{
    public List<Renderer> Renderers = new List<Renderer>();

    public bool IsVisible = false;

    // Use this for initialization
    void Start()
    {
        if (GetComponent<Renderer>() != null)
            Renderers.Add(GetComponent<Renderer>());

        foreach(Renderer rend in GetComponentsInChildren<Renderer>())
        {
            Renderers.Add(rend);
        }

    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);
        //bool onScreen =  screenPoint.x > -0.5 && screenPoint.x < 1.5 && screenPoint.y > -0.5 && screenPoint.y < 1.5;

        //if(onScreen && !IsVisible)
        //{
        //    SwitchAllRenderers(true);
        //    IsVisible = true;
        //}

        //if(!onScreen && IsVisible)
        //{
        //    SwitchAllRenderers(false);
        //    IsVisible = false;
        //}
    }

    public void SwitchAllRenderers(bool enable)
    {
        foreach(Renderer rend in Renderers)
        {
            rend.enabled = enable;
        }
    }
}
